#!/usr/bin/env python3
# Copyright (C) 2018 Quick2Space.org under the MIT License (MIT)
# See the LICENSE.txt file in the project root for more information.

import threading
from MCP25625_api import MCP25625_api, Message
import io
from PIL import Image

class can_sample(object):

    def __init__(self):
        # Setup Listener thread
        self.listening = threading.Event()
        self.listening.set()    # clear to stop listening
        self.listenThread = threading.Thread(target=self.ListenThread)
        self.listenThread.daemon = True

        # Setup CAN API
        self.can = MCP25625_api() #verbosePrint = True) - Set this to get more debugging information.
        self.can.Initialize()

        self.can.SetNormalMode() # Uncomment for normal mode.
        #self.can.SetLoopbackMode() # Uncomment for loopback (debug) mode.

    def Start(self):
        # Start Listener thread
        self.listenThread.start()

        while(True):
            input("----: Press Ctrl+C to stop or ENTER to send another CAN message.")

    def ListenThread(self):
        print("RECV: Listening for CAN messages...")

        datacount = 0
        packetcount = 0
        firstPacket = True
        alldata = []
        while (self.listening.is_set()):
            try:
                recvMsg = self.can.Recv(timeoutMilliseconds=None)
                msg = str(recvMsg).split(" ")[4:]
                packet = []
                for byte in msg:
                    try:
                        if not byte == "":
                            packet.append(int(byte, 16))
                            alldata.append(int(byte, 16))
                            datacount += 1
                    except ValueError as e:
                        print("Error message: " + str(e))

                packetcount+=1

                if packet == [0,0,0,0,0,0,0,0]:
                    print("Got " + str(packetcount) + " lines of data")
                    print("Should have sent: " + str(datacount) + " bytes of data")
                    break

                if firstPacket:
                    print(packet)
                    firstPacket = False

            except TimeoutError as e:
                print("\nRECV: Timeout receiving message. <{0}>".format(e))

        print("RECV: Listener stopped.")

        alldata = bytearray(alldata)
        image = Image.open(io.BytesIO(alldata))
        image.save("recvPic.jpg")

if __name__ == "__main__":
    sample = can_sample()
    sample.Start()
