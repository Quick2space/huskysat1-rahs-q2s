import os
import datetime
import shutil
from time import sleep
from picamera import PiCamera
from PIL import Image

os.chdir(os.getcwd())

def takePic():
    camera = PiCamera()
    camera.resolution = (640, 480)
    camera.vflip = True
    #preview only needed for testing
    #camera.start_preview() #comment out for real
    #sleep(5) #definitly comment out for real
    camera.capture('pic.jpg')
    compress('pic.jpg')
    #camera.stop_preview()

def compress(file):
    filepath = os.path.join(os.getcwd(), file)
    oldsize = os.stat(filepath).st_size

    picture = Image.open(filepath)
    dim = picture.size

    picture.save(file)

    newsize = os.stat(os.path.join(os.getcwd(), file)).st_size

    percent = (oldsize-newsize)/float(oldsize)*100

    print("File compressed from " + str(oldsize) + " to " + str(newsize) + " or " + str(percent) + "%") 
def checkDir():
    if not os.path.isdir("photoArchive"):
        os.makedirs("photoArchive")

def moveAndRenamePic():
    curTime = str(datetime.datetime.now()).split(" ")
    name = curTime[0] + "_" + curTime[1].split(".")[0]
    os.popen("cp pic.jpg photoArchive/"+str(name)+".jpg")

takePic()
checkDir()
moveAndRenamePic()



"""
We still need to:
    check to see if there is or isn't an archive folder
        if there isn't, make one

    move the picture to archive and remane it to "day_month_year_hour_minute.jpg"

    (we will probably want some sort of "check size of folder and delete old pics/compress/something to reduce size)

    start the sending code in a good way
        ###COMPLETED###

Remember, all of this needs to be done before Tuesday next week
"""

sleep(1) #pause between taking/modifying/handling/etc. picture and running the send code





""" Dont touch anything below this comment, just do all your work above here"""

import PicSend
from PIL import Image
import io
def lookAtPic():
    with open("pic.jpg", "rb") as file:
        allbytes = file.read()
    packet = []
    packetcount = 0
    data = []
    datacount = 0
    for byte in allbytes:
        datacount += 1
        data.append(byte)
        packet.append(byte)
        if len(packet) == 8:
            packet = []
            packetcount += 1
    packetcount = float(packetcount)
    packetcount += len(packet)/8

    """This code is only to check to make sure the picture copying/data method works"""
    #data = bytearray(data)
    #image = Image.open(io.BytesIO(data))
    #image.save("copy.jpg")
    
    print("There are: " + str(packetcount) + " packets (8-byte arrays) in pic.jpg")
    print("The last packet only has " + str(len(packet)) + " bytes in it")
    print("There are: " + str(datacount) + " pieces of data in pic.jpg")
    print(str(datacount/8) + " should equal " + str(packetcount) + " --> " + str(float(datacount/8) == float(packetcount)))
    print(str(packetcount*8) + " should equal " + str(datacount) + " --> " + str(float(packetcount*8) == float(datacount)))
    print("Because average send time is 0.1 seconds/packet right now, expected send time will be: " + str((packetcount*0.1)/60) + " minutes. Sorry.")

lookAtPic()

import PicSend
PicSend.PicSendMain()
print("Should be sending now")


