<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="11" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="no"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="no"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="no"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="no"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="no"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="no"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="110" color="7" fill="1" visible="no" active="no"/>
<layer number="111" name="111" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="no"/>
<layer number="117" name="mPads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="mUnrouted" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="mDimension" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="mbStop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="no" active="no"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="14" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="no"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="no"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="no"/>
<layer number="207" name="207bmp" color="15" fill="10" visible="no" active="no"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="HELP" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ICs">
<packages>
<package name="SSOP-28">
<smd name="P$1" x="-4.225" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$2" x="-3.575" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$3" x="-2.925" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$4" x="-2.275" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$5" x="-1.625" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$6" x="-0.975" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$7" x="-0.325" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$8" x="0.325" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$9" x="0.975" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$10" x="1.625" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$11" x="2.275" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$12" x="2.925" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$13" x="3.575" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$14" x="4.225" y="-3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$15" x="4.225" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$16" x="3.575" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$17" x="2.925" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$18" x="2.275" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$19" x="1.625" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$20" x="0.975" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$21" x="0.325" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$22" x="-0.325" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$23" x="-0.975" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$24" x="-1.625" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$25" x="-2.275" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$26" x="-2.925" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$27" x="-3.575" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<smd name="P$28" x="-4.225" y="3.6" dx="0.45" dy="1.75" layer="1"/>
<wire x1="-5.1" y1="-2.65" x2="-5.1" y2="2.65" width="0.127" layer="21"/>
<wire x1="-5.1" y1="2.65" x2="5.1" y2="2.65" width="0.127" layer="21"/>
<wire x1="5.1" y1="2.65" x2="5.1" y2="-2.65" width="0.127" layer="21"/>
<wire x1="5.1" y1="-2.65" x2="-5.1" y2="-2.65" width="0.127" layer="21"/>
<text x="-4.6" y="0.8" size="1.27" layer="25" font="vector">MCP25625</text>
<circle x="-4.5" y="-2" radius="0.2" width="0.5" layer="21"/>
</package>
<package name="SMD_CRYSTAL">
<smd name="P$1" x="-1.85" y="-1.1" dx="1.6" dy="1.25" layer="1"/>
<smd name="P$2" x="1.85" y="-1.1" dx="1.6" dy="1.25" layer="1"/>
<smd name="P$3" x="1.85" y="1.1" dx="1.6" dy="1.25" layer="1"/>
<smd name="P$4" x="-1.85" y="1.1" dx="1.6" dy="1.25" layer="1"/>
<wire x1="-2.5" y1="2" x2="2.5" y2="2" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2" x2="-2.5" y2="-2" width="0.127" layer="21"/>
</package>
<package name="8-LFCSP">
<smd name="P$1" x="-1.4" y="0.75" dx="0.6" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="P$2" x="-1.4" y="0.25" dx="0.6" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="P$3" x="-1.4" y="-0.25" dx="0.6" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="P$4" x="-1.4" y="-0.75" dx="0.6" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="P$5" x="1.4" y="-0.75" dx="0.6" dy="0.24" layer="1" roundness="100"/>
<smd name="P$6" x="1.4" y="-0.25" dx="0.6" dy="0.24" layer="1" roundness="100"/>
<smd name="P$7" x="1.4" y="0.25" dx="0.6" dy="0.24" layer="1" roundness="100"/>
<smd name="P$8" x="1.4" y="0.75" dx="0.6" dy="0.24" layer="1" roundness="100"/>
<wire x1="-1.5" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<smd name="P$9" x="0" y="0" dx="2.44" dy="1.6" layer="1" roundness="8" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="MCP25625_SSOP">
<pin name="VIO" x="-17.78" y="0" length="middle"/>
<pin name="NC" x="-17.78" y="-5.08" length="middle"/>
<pin name="CANL" x="-17.78" y="-10.16" length="middle"/>
<pin name="CANH" x="-17.78" y="-15.24" length="middle"/>
<pin name="STBY" x="-17.78" y="-20.32" length="middle"/>
<pin name="TX1RTS" x="-17.78" y="-25.4" length="middle"/>
<pin name="TX2RTS" x="-17.78" y="-30.48" length="middle"/>
<pin name="OSC2" x="-17.78" y="-35.56" length="middle"/>
<pin name="OSC1" x="-17.78" y="-40.64" length="middle"/>
<pin name="GND" x="-17.78" y="-45.72" length="middle"/>
<pin name="RX1BF" x="-17.78" y="-50.8" length="middle"/>
<pin name="RX0BF" x="-17.78" y="-55.88" length="middle"/>
<pin name="INT" x="-17.78" y="-60.96" length="middle"/>
<pin name="SCK" x="-17.78" y="-66.04" length="middle"/>
<pin name="SI" x="17.78" y="-66.04" length="middle" rot="R180"/>
<pin name="SO" x="17.78" y="-60.96" length="middle" rot="R180"/>
<pin name="CS" x="17.78" y="-55.88" length="middle" rot="R180"/>
<pin name="RESET" x="17.78" y="-50.8" length="middle" rot="R180"/>
<pin name="VDD" x="17.78" y="-45.72" length="middle" rot="R180"/>
<pin name="TXCAN" x="17.78" y="-40.64" length="middle" rot="R180"/>
<pin name="RXCAN" x="17.78" y="-35.56" length="middle" rot="R180"/>
<pin name="CLKOUT" x="17.78" y="-30.48" length="middle" rot="R180"/>
<pin name="TX0RTS" x="17.78" y="-25.4" length="middle" rot="R180"/>
<pin name="TXD" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="NC25" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="VSS" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="VDDA" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="RXD" x="17.78" y="0" length="middle" rot="R180"/>
<wire x1="-12.7" y1="-68.58" x2="-12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="-12.7" y1="2.54" x2="12.7" y2="2.54" width="0.254" layer="94"/>
<wire x1="12.7" y1="2.54" x2="12.7" y2="-68.58" width="0.254" layer="94"/>
<wire x1="12.7" y1="-68.58" x2="-12.7" y2="-68.58" width="0.254" layer="94"/>
<text x="-8.89" y="-71.882" size="2.54" layer="95">MCP25625</text>
</symbol>
<symbol name="ECX-53B-DU">
<pin name="P$1" x="-12.7" y="-2.54" visible="off" length="middle"/>
<pin name="P$2" x="12.7" y="-2.54" visible="off" length="middle" rot="R180"/>
<pin name="P$3" x="12.7" y="2.54" visible="off" length="middle" rot="R180"/>
<pin name="P$4" x="-12.7" y="2.54" visible="off" length="middle"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<text x="-7.62" y="-7.62" size="1.778" layer="95">ECX-53B-DU</text>
</symbol>
<symbol name="ADM7172">
<pin name="VOUT1" x="-15.24" y="7.62" length="middle"/>
<pin name="VOUT2" x="-15.24" y="2.54" length="middle"/>
<pin name="SENSE" x="-15.24" y="-2.54" length="middle"/>
<pin name="SS" x="-15.24" y="-7.62" length="middle"/>
<pin name="EN" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="GND" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="VIN2" x="15.24" y="2.54" length="middle" rot="R180"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<pin name="VIN1" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="GNDP" x="15.24" y="-12.7" length="middle" rot="R180"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<text x="-9.652" y="-13.462" size="1.778" layer="95">ADM7172</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MICROCHIP_CAN_MCP25625_SSOP">
<gates>
<gate name="G$1" symbol="MCP25625_SSOP" x="0" y="35.56"/>
</gates>
<devices>
<device name="" package="SSOP-28">
<connects>
<connect gate="G$1" pin="CANH" pad="P$4"/>
<connect gate="G$1" pin="CANL" pad="P$3"/>
<connect gate="G$1" pin="CLKOUT" pad="P$22"/>
<connect gate="G$1" pin="CS" pad="P$17"/>
<connect gate="G$1" pin="GND" pad="P$10"/>
<connect gate="G$1" pin="INT" pad="P$13"/>
<connect gate="G$1" pin="NC" pad="P$2"/>
<connect gate="G$1" pin="NC25" pad="P$25"/>
<connect gate="G$1" pin="OSC1" pad="P$9"/>
<connect gate="G$1" pin="OSC2" pad="P$8"/>
<connect gate="G$1" pin="RESET" pad="P$18"/>
<connect gate="G$1" pin="RX0BF" pad="P$12"/>
<connect gate="G$1" pin="RX1BF" pad="P$11"/>
<connect gate="G$1" pin="RXCAN" pad="P$21"/>
<connect gate="G$1" pin="RXD" pad="P$28"/>
<connect gate="G$1" pin="SCK" pad="P$14"/>
<connect gate="G$1" pin="SI" pad="P$15"/>
<connect gate="G$1" pin="SO" pad="P$16"/>
<connect gate="G$1" pin="STBY" pad="P$5"/>
<connect gate="G$1" pin="TX0RTS" pad="P$23"/>
<connect gate="G$1" pin="TX1RTS" pad="P$6"/>
<connect gate="G$1" pin="TX2RTS" pad="P$7"/>
<connect gate="G$1" pin="TXCAN" pad="P$20"/>
<connect gate="G$1" pin="TXD" pad="P$24"/>
<connect gate="G$1" pin="VDD" pad="P$19"/>
<connect gate="G$1" pin="VDDA" pad="P$27"/>
<connect gate="G$1" pin="VIO" pad="P$1"/>
<connect gate="G$1" pin="VSS" pad="P$26"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ECS_CRYSTAL_ECX-53B-DU">
<gates>
<gate name="G$1" symbol="ECX-53B-DU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMD_CRYSTAL">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ANALOG_LDO_ADM7172">
<gates>
<gate name="G$1" symbol="ADM7172" x="0" y="0"/>
</gates>
<devices>
<device name="" package="8-LFCSP">
<connects>
<connect gate="G$1" pin="EN" pad="P$5"/>
<connect gate="G$1" pin="GND" pad="P$6"/>
<connect gate="G$1" pin="GNDP" pad="P$9"/>
<connect gate="G$1" pin="SENSE" pad="P$3"/>
<connect gate="G$1" pin="SS" pad="P$4"/>
<connect gate="G$1" pin="VIN1" pad="P$8"/>
<connect gate="G$1" pin="VIN2" pad="P$7"/>
<connect gate="G$1" pin="VOUT1" pad="P$1"/>
<connect gate="G$1" pin="VOUT2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Aesthetics">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find non-functional items- supply symbols, logos, notations, frame blocks, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="5V">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="3.3V">
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<text x="-1.016" y="3.556" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="DGND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="5V" prefix="SUPPLY">
<description>5V supply symbol</description>
<gates>
<gate name="G$1" symbol="5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DGND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="DGND" x="0" y="2.54"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Capacitors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="0.508" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="0.254" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL2">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
</package>
<package name="0805">
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="1.70230625" y="-0.185675" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<wire x1="-1.5" y1="0.8" x2="1.5" y2="0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="0.8" x2="1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="1.5" y1="-0.8" x2="-1.5" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-0.8" x2="-1.5" y2="0.8" width="0.127" layer="21"/>
</package>
<package name="CAP-PTH-LARGE">
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4.826" y="0" drill="0.9" diameter="1.905"/>
<pad name="2" x="4.572" y="0" drill="0.9" diameter="1.905"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="GRM43D">
<wire x1="2.25" y1="1.6" x2="1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.3" y1="1.8" x2="2.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.8" x2="2.3" y2="-1.8" width="0.127" layer="21"/>
<smd name="A" x="1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<smd name="C" x="-1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<text x="-2" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-2" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.2" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="2.2" y2="1.6" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-1.4603" y1="0.5766" x2="1.4603" y2="0.5766" width="0.127" layer="21"/>
<wire x1="1.4603" y1="0.5766" x2="1.4603" y2="-0.5893" width="0.127" layer="21"/>
<wire x1="1.4603" y1="-0.5893" x2="-1.4603" y2="-0.5893" width="0.127" layer="21"/>
<wire x1="-1.4603" y1="-0.5893" x2="-1.4603" y2="0.5766" width="0.127" layer="21"/>
<smd name="1" x="-0.725" y="0" dx="0.95" dy="0.75" layer="1"/>
<smd name="2" x="0.725" y="0" dx="0.95" dy="0.75" layer="1"/>
<text x="1.6002" y="-0.2032" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.882" y1="0.4322" x2="0.882" y2="0.4322" width="0.0254" layer="21"/>
<wire x1="0.882" y1="0.4322" x2="0.882" y2="-0.4322" width="0.0254" layer="21"/>
<wire x1="0.882" y1="-0.4322" x2="-0.882" y2="-0.4322" width="0.0254" layer="21"/>
<wire x1="-0.882" y1="-0.4322" x2="-0.882" y2="0.4322" width="0.0254" layer="21"/>
<smd name="1" x="-0.425" y="0" dx="0.6" dy="0.55" layer="1"/>
<smd name="2" x="0.425" y="0" dx="0.6" dy="0.55" layer="1"/>
<text x="0.9463375" y="-0.157478125" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-5MM">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.5" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.4" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-1.435" y="-2.675" size="1.016" layer="25" font="vector">&gt;NAME</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="21"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="21"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="21"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="21"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="CTZ3">
<description>CTZ3 Series land pattern for variable capacitor - CTZ3E-50C-W1-PF</description>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="-1" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.2" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.05" y1="2.25" x2="-1.7" y2="1.45" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.45" x2="-1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.35" x2="-1.05" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.05" y1="2.25" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.35" x2="1.05" y2="-2.35" width="0.127" layer="21"/>
<smd name="+" x="0" y="2.05" dx="1.5" dy="1.2" layer="1"/>
<smd name="-" x="0" y="-2.05" dx="1.5" dy="1.2" layer="1"/>
<text x="-2" y="3" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2" y="-3.4" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="CAP-PTH-SMALLEZ">
<description>This is the "EZ" version of the .1" spaced ceramic thru-hole cap.&lt;br&gt;
It has reduced top mask to make it harder to put the component on the wrong side of the board.</description>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651" stop="no"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" stop="no"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
<circle x="0" y="0" radius="0.898025" width="0" layer="30"/>
<circle x="2.54" y="0" radius="0.915809375" width="0" layer="30"/>
<circle x="0" y="0" radius="0.40160625" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.40160625" width="0" layer="29"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CAP-PTH-SMALL2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CAP-PTH-LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="GRM43D">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CAP-PTH-5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ASMD" package="CTZ3">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="CAP-PTH-SMALLEZ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Resistors">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.9015" y1="0.4195" x2="0.9015" y2="0.4195" width="0.0508" layer="21"/>
<wire x1="0.9015" y1="0.4195" x2="0.9015" y2="-0.4195" width="0.0508" layer="21"/>
<wire x1="0.9015" y1="-0.4195" x2="-0.9015" y2="-0.4195" width="0.0508" layer="21"/>
<wire x1="-0.9015" y1="-0.4195" x2="-0.9015" y2="0.4195" width="0.0508" layer="21"/>
<smd name="1" x="-0.425" y="0" dx="0.6" dy="0.55" layer="1"/>
<smd name="2" x="0.425" y="0" dx="0.6" dy="0.55" layer="1"/>
<text x="0.9652" y="-0.1651" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="21"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="21"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="21"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="21"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="0.3175" y="1.905" size="1.016" layer="25" font="vector" rot="R90">&gt;NAME</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="1.5113" y="-0.127" size="0.4064" layer="25" font="vector">&gt;NAME</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.729" x2="1.473" y2="0.729" width="0.0508" layer="21"/>
<wire x1="1.473" y1="0.729" x2="1.473" y2="-0.729" width="0.0508" layer="21"/>
<wire x1="1.473" y1="-0.729" x2="-1.473" y2="-0.729" width="0.0508" layer="21"/>
<wire x1="-1.473" y1="-0.729" x2="-1.473" y2="0.729" width="0.0508" layer="21"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="1.651" y="-0.1778" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762" diameter="1.6764"/>
<pad name="2" x="2.5" y="0" drill="0.762" diameter="1.6764"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
</package>
<package name="AXIAL-0.1EZ">
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.524" y1="0" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="2.54" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="0" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="0" y="-2.159" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="0" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="0" y="0" radius="1.016" width="0" layer="30"/>
<circle x="2.54" y="0" radius="1.016" width="0" layer="30"/>
<circle x="0" y="0" radius="0.4572" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.4572" width="0" layer="29"/>
</package>
<package name="AXIAL-0.1">
<wire x1="1.27" y1="-0.762" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.524" y1="0" x2="1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0" x2="1.016" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="0" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.54" y="0" drill="0.9" diameter="1.8796"/>
<text x="0" y="1.27" size="1.016" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="0" y="-2.159" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W-VERT-KIT" package="AXIAL-0.1EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W-VERT" package="AXIAL-0.1">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="RPI-Zero">
<description>Generated from &lt;b&gt;TMC2660-test-with-grbl.brd&lt;/b&gt;&lt;p&gt;
by exp-project-lbr.ulp</description>
<packages>
<package name="RASPBERRYPI_BASTELSTUBE_V13_RASPI_BOARD_B+_FULL">
<description>Raspberry Pi board model B+, full outline with position of big connectors &amp;amp; drill holes</description>
<circle x="3.5" y="3.5" radius="3.1" width="0.127" layer="52"/>
<circle x="61.5" y="3.5" radius="3.1" width="0.127" layer="52"/>
<circle x="61.5" y="26.5" radius="3.1" width="0.127" layer="52"/>
<circle x="3.5" y="26.5" radius="3.1" width="0.127" layer="52"/>
<wire x1="0" y1="3" x2="3" y2="0" width="0.254" layer="52" curve="90"/>
<wire x1="3" y1="0" x2="6.85" y2="0" width="0.254" layer="52"/>
<wire x1="6.85" y1="0" x2="14.35" y2="0" width="0.254" layer="52"/>
<wire x1="14.35" y1="0" x2="62" y2="0" width="0.254" layer="52"/>
<wire x1="62" y1="0" x2="65" y2="3" width="0.254" layer="52" curve="90"/>
<wire x1="65" y1="3" x2="65" y2="27" width="0.254" layer="52"/>
<wire x1="65" y1="27" x2="62" y2="30" width="0.254" layer="52" curve="90"/>
<wire x1="62" y1="30" x2="3" y2="30" width="0.254" layer="52"/>
<wire x1="3" y1="30" x2="0" y2="27" width="0.254" layer="52" curve="90"/>
<wire x1="0" y1="27" x2="0" y2="3" width="0.254" layer="52"/>
<wire x1="7.1" y1="29.04" x2="7.1" y2="26.5" width="0.127" layer="21"/>
<wire x1="7.1" y1="26.5" x2="7.1" y2="23.96" width="0.127" layer="21"/>
<wire x1="7.1" y1="23.96" x2="8.0525" y2="23.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="23.96" x2="8.6875" y2="23.96" width="0.127" layer="21"/>
<wire x1="8.6875" y1="23.96" x2="9.64" y2="23.96" width="0.127" layer="21"/>
<wire x1="9.64" y1="23.96" x2="57.9" y2="23.96" width="0.127" layer="21"/>
<wire x1="57.9" y1="23.96" x2="57.9" y2="29.04" width="0.127" layer="21"/>
<wire x1="57.9" y1="29.04" x2="7.1" y2="29.04" width="0.127" layer="21"/>
<wire x1="7.1" y1="26.5" x2="9.64" y2="26.5" width="0.127" layer="21"/>
<wire x1="9.64" y1="26.5" x2="9.64" y2="23.96" width="0.127" layer="21"/>
<wire x1="8.0525" y1="23.96" x2="8.0525" y2="23.6425" width="0.127" layer="21"/>
<wire x1="8.0525" y1="23.6425" x2="8.6875" y2="23.6425" width="0.127" layer="21"/>
<wire x1="8.6875" y1="23.6425" x2="8.6875" y2="23.96" width="0.127" layer="21"/>
<wire x1="6.85" y1="0" x2="6.85" y2="5.75" width="0.127" layer="52"/>
<wire x1="6.85" y1="5.75" x2="14.35" y2="5.75" width="0.127" layer="52"/>
<wire x1="14.35" y1="5.75" x2="14.35" y2="0" width="0.127" layer="52"/>
<wire x1="6.85" y1="0" x2="6.6" y2="-0.5" width="0.127" layer="52"/>
<wire x1="6.6" y1="-0.5" x2="14.6" y2="-0.5" width="0.127" layer="52"/>
<wire x1="14.6" y1="-0.5" x2="14.35" y2="0" width="0.127" layer="52"/>
<pad name="1" x="8.37" y="25.23" drill="1" diameter="1.778" shape="square"/>
<pad name="2" x="8.37" y="27.77" drill="1" diameter="1.778"/>
<pad name="3" x="10.91" y="25.23" drill="1" diameter="1.778"/>
<pad name="4" x="10.91" y="27.77" drill="1" diameter="1.778"/>
<pad name="5" x="13.45" y="25.23" drill="1" diameter="1.778"/>
<pad name="6" x="13.45" y="27.77" drill="1" diameter="1.778"/>
<pad name="7" x="15.99" y="25.23" drill="1" diameter="1.778"/>
<pad name="8" x="15.99" y="27.77" drill="1" diameter="1.778"/>
<pad name="9" x="18.53" y="25.23" drill="1" diameter="1.778"/>
<pad name="10" x="18.53" y="27.77" drill="1" diameter="1.778"/>
<pad name="11" x="21.07" y="25.23" drill="1" diameter="1.778"/>
<pad name="12" x="21.07" y="27.77" drill="1" diameter="1.778"/>
<pad name="13" x="23.61" y="25.23" drill="1" diameter="1.778"/>
<pad name="14" x="23.61" y="27.77" drill="1" diameter="1.778"/>
<pad name="15" x="26.15" y="25.23" drill="1" diameter="1.778"/>
<pad name="16" x="26.15" y="27.77" drill="1" diameter="1.778"/>
<pad name="17" x="28.69" y="25.23" drill="1" diameter="1.778"/>
<pad name="18" x="28.69" y="27.77" drill="1" diameter="1.778"/>
<pad name="19" x="31.23" y="25.23" drill="1" diameter="1.778"/>
<pad name="20" x="31.23" y="27.77" drill="1" diameter="1.778"/>
<pad name="21" x="33.77" y="25.23" drill="1" diameter="1.778"/>
<pad name="22" x="33.77" y="27.77" drill="1" diameter="1.778"/>
<pad name="23" x="36.31" y="25.23" drill="1" diameter="1.778"/>
<pad name="24" x="36.31" y="27.77" drill="1" diameter="1.778"/>
<pad name="25" x="38.85" y="25.23" drill="1" diameter="1.778"/>
<pad name="26" x="38.85" y="27.77" drill="1" diameter="1.778"/>
<pad name="27" x="41.39" y="25.23" drill="1" diameter="1.778"/>
<pad name="28" x="41.39" y="27.77" drill="1" diameter="1.778"/>
<pad name="29" x="43.93" y="25.23" drill="1" diameter="1.778"/>
<pad name="30" x="43.93" y="27.77" drill="1" diameter="1.778"/>
<pad name="31" x="46.47" y="25.23" drill="1" diameter="1.778"/>
<pad name="32" x="46.47" y="27.77" drill="1" diameter="1.778"/>
<pad name="33" x="49.01" y="25.23" drill="1" diameter="1.778"/>
<pad name="34" x="49.01" y="27.77" drill="1" diameter="1.778"/>
<pad name="35" x="51.55" y="25.23" drill="1" diameter="1.778"/>
<pad name="36" x="51.55" y="27.77" drill="1" diameter="1.778"/>
<pad name="37" x="54.09" y="25.23" drill="1" diameter="1.778"/>
<pad name="38" x="54.09" y="27.77" drill="1" diameter="1.778"/>
<pad name="39" x="56.63" y="25.23" drill="1" diameter="1.778"/>
<pad name="40" x="56.63" y="27.77" drill="1" diameter="1.778"/>
<text x="8.0525" y="22.3725" size="1.016" layer="21" font="vector" ratio="10">1</text>
<hole x="3.5" y="3.5" drill="2.75"/>
<hole x="3.5" y="26.5" drill="2.75"/>
<hole x="61.5" y="3.5" drill="2.75"/>
<hole x="61.5" y="26.5" drill="2.75"/>
<wire x1="36.9" y1="0" x2="37.75" y2="0" width="0.254" layer="52"/>
<wire x1="37.75" y1="0" x2="45.25" y2="0" width="0.254" layer="52"/>
<wire x1="37.75" y1="0" x2="37.75" y2="5.75" width="0.127" layer="52"/>
<wire x1="37.75" y1="5.75" x2="45.25" y2="5.75" width="0.127" layer="52"/>
<wire x1="45.25" y1="5.75" x2="45.25" y2="0" width="0.127" layer="52"/>
<wire x1="37.75" y1="0" x2="37.5" y2="-0.5" width="0.127" layer="52"/>
<wire x1="37.5" y1="-0.5" x2="45.5" y2="-0.5" width="0.127" layer="52"/>
<wire x1="45.5" y1="-0.5" x2="45.25" y2="0" width="0.127" layer="52"/>
<wire x1="49.5" y1="0" x2="50.35" y2="0" width="0.254" layer="52"/>
<wire x1="50.35" y1="0" x2="57.85" y2="0" width="0.254" layer="52"/>
<wire x1="50.35" y1="0" x2="50.35" y2="5.75" width="0.127" layer="52"/>
<wire x1="50.35" y1="5.75" x2="57.85" y2="5.75" width="0.127" layer="52"/>
<wire x1="57.85" y1="5.75" x2="57.85" y2="0" width="0.127" layer="52"/>
<wire x1="50.35" y1="0" x2="50.1" y2="-0.5" width="0.127" layer="52"/>
<wire x1="50.1" y1="-0.5" x2="58.1" y2="-0.5" width="0.127" layer="52"/>
<wire x1="58.1" y1="-0.5" x2="57.85" y2="0" width="0.127" layer="52"/>
<wire x1="64.618" y1="6.886" x2="61.218" y2="6.886" width="0.1524" layer="21"/>
<wire x1="61.218" y1="6.886" x2="61.218" y2="23.086" width="0.1524" layer="21"/>
<wire x1="61.218" y1="23.086" x2="64.618" y2="23.086" width="0.1524" layer="21"/>
<wire x1="64.618" y1="23.086" x2="64.618" y2="6.886" width="0.1524" layer="21"/>
<wire x1="65.768" y1="6.886" x2="64.618" y2="6.886" width="0.1524" layer="21"/>
<wire x1="64.618" y1="23.086" x2="65.768" y2="23.086" width="0.1524" layer="21"/>
<wire x1="65.768" y1="23.086" x2="65.768" y2="6.886" width="0.1524" layer="21"/>
<wire x1="-2" y1="23" x2="-2" y2="12" width="0.127" layer="21"/>
<wire x1="8" y1="12" x2="8" y2="23" width="0.127" layer="21"/>
<wire x1="8" y1="23" x2="-2" y2="23" width="0.127" layer="21"/>
<wire x1="-2" y1="12" x2="8" y2="12" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RPI-ZERO">
<wire x1="-20.32" y1="33.02" x2="22.86" y2="33.02" width="0.254" layer="94"/>
<wire x1="22.86" y1="33.02" x2="22.86" y2="-27.94" width="0.254" layer="94"/>
<wire x1="22.86" y1="-27.94" x2="12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="-10.16" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-27.94" x2="-20.32" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-27.94" x2="-20.32" y2="33.02" width="0.254" layer="94"/>
<pin name="5V0@1" x="25.4" y="30.48" length="short" direction="sup" rot="R180"/>
<pin name="3V3PI@1" x="-22.86" y="30.48" length="short" direction="sup"/>
<pin name="GPIO2/SDA1" x="-22.86" y="27.94" length="short"/>
<pin name="5V0@2" x="25.4" y="27.94" length="short" direction="sup" rot="R180"/>
<pin name="GPIO3/SCL1" x="-22.86" y="25.4" length="short"/>
<pin name="GND@8" x="25.4" y="25.4" length="short" direction="sup" rot="R180"/>
<pin name="GPIO4/GCKL" x="-22.86" y="22.86" length="short"/>
<pin name="TXD0/GPIO14" x="25.4" y="22.86" length="short" rot="R180"/>
<pin name="GND@1" x="-22.86" y="20.32" length="short" direction="sup"/>
<pin name="RXD0/GPIO15" x="25.4" y="20.32" length="short" rot="R180"/>
<pin name="GPIO17/GEN0" x="-22.86" y="17.78" length="short"/>
<pin name="GPIO18" x="25.4" y="17.78" length="short" rot="R180"/>
<pin name="GPIO27/GEN2" x="-22.86" y="15.24" length="short"/>
<pin name="GND@4" x="25.4" y="15.24" length="short" direction="sup" rot="R180"/>
<pin name="GPIO22/GEN3" x="-22.86" y="12.7" length="short"/>
<pin name="GEN4/GPIO23" x="25.4" y="12.7" length="short" rot="R180"/>
<pin name="3V3PI@2" x="-22.86" y="10.16" length="short" direction="sup"/>
<pin name="GEN5/GPIO24" x="25.4" y="10.16" length="short" rot="R180"/>
<pin name="GPIO10/MOSI" x="-22.86" y="7.62" length="short"/>
<pin name="GND@5" x="25.4" y="7.62" length="short" direction="sup" rot="R180"/>
<pin name="GPIO9/MISO" x="-22.86" y="5.08" length="short"/>
<pin name="GEN/6GPIO25" x="25.4" y="5.08" length="short" rot="R180"/>
<pin name="GPIO11/SCLK" x="-22.86" y="2.54" length="short"/>
<pin name="!CE0!/GPIO8" x="25.4" y="2.54" length="short" rot="R180"/>
<pin name="GND@2" x="-22.86" y="0" length="short" direction="sup"/>
<pin name="!CE!/GPIO7" x="25.4" y="0" length="short" rot="R180"/>
<pin name="ID_SD" x="-22.86" y="-2.54" length="short"/>
<pin name="ID_SC" x="25.4" y="-2.54" length="short" rot="R180"/>
<pin name="GPIO5" x="-22.86" y="-5.08" length="short"/>
<pin name="GND@6" x="25.4" y="-5.08" length="short" direction="sup" rot="R180"/>
<pin name="GPIO6" x="-22.86" y="-7.62" length="short"/>
<pin name="GPIO12" x="25.4" y="-7.62" length="short" rot="R180"/>
<pin name="GPIO13" x="-22.86" y="-10.16" length="short"/>
<pin name="GND@7" x="25.4" y="-10.16" length="short" direction="sup" rot="R180"/>
<pin name="GPIO19" x="-22.86" y="-12.7" length="short"/>
<pin name="GPIO16" x="25.4" y="-12.7" length="short" rot="R180"/>
<pin name="GPIO26" x="-22.86" y="-15.24" length="short"/>
<pin name="GPIO20" x="25.4" y="-15.24" length="short" rot="R180"/>
<pin name="GND@3" x="-22.86" y="-17.78" length="short" direction="sup"/>
<pin name="GPIO21" x="25.4" y="-17.78" length="short" rot="R180"/>
<text x="-20.32" y="35.56" size="1.27" layer="95" font="vector">&gt;NAME</text>
<text x="-20.32" y="33.655" size="1.27" layer="96" font="vector">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RPI-ZERO">
<gates>
<gate name="G$1" symbol="RPI-ZERO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RASPBERRYPI_BASTELSTUBE_V13_RASPI_BOARD_B+_FULL">
<connects>
<connect gate="G$1" pin="!CE!/GPIO7" pad="26"/>
<connect gate="G$1" pin="!CE0!/GPIO8" pad="24"/>
<connect gate="G$1" pin="3V3PI@1" pad="1"/>
<connect gate="G$1" pin="3V3PI@2" pad="17"/>
<connect gate="G$1" pin="5V0@1" pad="2"/>
<connect gate="G$1" pin="5V0@2" pad="4"/>
<connect gate="G$1" pin="GEN/6GPIO25" pad="22"/>
<connect gate="G$1" pin="GEN4/GPIO23" pad="16"/>
<connect gate="G$1" pin="GEN5/GPIO24" pad="18"/>
<connect gate="G$1" pin="GND@1" pad="9"/>
<connect gate="G$1" pin="GND@2" pad="25"/>
<connect gate="G$1" pin="GND@3" pad="39"/>
<connect gate="G$1" pin="GND@4" pad="14"/>
<connect gate="G$1" pin="GND@5" pad="20"/>
<connect gate="G$1" pin="GND@6" pad="30"/>
<connect gate="G$1" pin="GND@7" pad="34"/>
<connect gate="G$1" pin="GND@8" pad="6"/>
<connect gate="G$1" pin="GPIO10/MOSI" pad="19"/>
<connect gate="G$1" pin="GPIO11/SCLK" pad="23"/>
<connect gate="G$1" pin="GPIO12" pad="32"/>
<connect gate="G$1" pin="GPIO13" pad="33"/>
<connect gate="G$1" pin="GPIO16" pad="36"/>
<connect gate="G$1" pin="GPIO17/GEN0" pad="11"/>
<connect gate="G$1" pin="GPIO18" pad="12"/>
<connect gate="G$1" pin="GPIO19" pad="35"/>
<connect gate="G$1" pin="GPIO2/SDA1" pad="3"/>
<connect gate="G$1" pin="GPIO20" pad="38"/>
<connect gate="G$1" pin="GPIO21" pad="40"/>
<connect gate="G$1" pin="GPIO22/GEN3" pad="15"/>
<connect gate="G$1" pin="GPIO26" pad="37"/>
<connect gate="G$1" pin="GPIO27/GEN2" pad="13"/>
<connect gate="G$1" pin="GPIO3/SCL1" pad="5"/>
<connect gate="G$1" pin="GPIO4/GCKL" pad="7"/>
<connect gate="G$1" pin="GPIO5" pad="29"/>
<connect gate="G$1" pin="GPIO6" pad="31"/>
<connect gate="G$1" pin="GPIO9/MISO" pad="21"/>
<connect gate="G$1" pin="ID_SC" pad="28"/>
<connect gate="G$1" pin="ID_SD" pad="27"/>
<connect gate="G$1" pin="RXD0/GPIO15" pad="10"/>
<connect gate="G$1" pin="TXD0/GPIO14" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DubSat1 Components">
<description>Coulomb counter

Datsheet: http://cds.linear.com/docs/en/datasheet/2943fa.pdf</description>
<packages>
<package name="0545482271">
<description>Victor's Part 2</description>
<wire x1="-8.1" y1="-3.15" x2="-8.1" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="-8.1" y1="-0.9" x2="-8.1" y2="0.25" width="0.1524" layer="21"/>
<wire x1="-8.1" y1="0.25" x2="8.1" y2="0.25" width="0.1524" layer="21"/>
<wire x1="8.1" y1="0.25" x2="8.1" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="8.1" y1="-0.9" x2="8.1" y2="-3.15" width="0.1524" layer="21"/>
<wire x1="8.1" y1="-3.15" x2="-8.1" y2="-3.15" width="0.1524" layer="21"/>
<smd name="11" x="-0.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="0.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="-0.75" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="-1.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-1.75" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="-2.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="-2.75" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="-3.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="-3.75" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="-4.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="2" x="-4.75" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="1" x="-5.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="0.75" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="1.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="1.75" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="2.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="2.75" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="18" x="3.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="19" x="3.75" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="20" x="4.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="21" x="4.75" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="22" x="5.25" y="0.5" dx="1" dy="0.3" layer="1" rot="R90"/>
<smd name="23" x="-7.525" y="-1.4" dx="1.65" dy="1.3" layer="1"/>
<smd name="24" x="7.525" y="-1.4" dx="1.65" dy="1.3" layer="1"/>
<wire x1="8.1" y1="-4.3" x2="-8.1" y2="-4.3" width="0.1524" layer="21"/>
<wire x1="-8.1" y1="-4.3" x2="-8.1" y2="-0.9" width="0.1524" layer="21"/>
<wire x1="8.1" y1="-0.9" x2="8.1" y2="-4.3" width="0.1524" layer="21"/>
<wire x1="8.1" y1="-4.3" x2="-8.1" y2="-4.3" width="0.1524" layer="21"/>
</package>
<package name="AMPHENOL_FCI_SFW15R-2STE1LF">
<description>Victor's Part</description>
<wire x1="-10.6" y1="4.5" x2="-10.6" y2="-4.5" width="0.1524" layer="51"/>
<wire x1="-10.6" y1="-4.5" x2="10.6" y2="-4.5" width="0.1524" layer="51"/>
<wire x1="10.6" y1="-4.5" x2="10.6" y2="4.5" width="0.1524" layer="51"/>
<wire x1="10.6" y1="4.5" x2="-10.6" y2="4.5" width="0.1524" layer="51"/>
<smd name="16" x="-8" y="0.8" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="17" x="8" y="0.8" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="8" x="0" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="1" x="-7" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="-6" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="3" x="-5" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="4" x="-4" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="5" x="-3" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="6" x="-2" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="7" x="-1" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="9" x="1" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="10" x="2" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="11" x="3" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="12" x="4" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="13" x="5" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="14" x="6" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
<smd name="15" x="7" y="3" dx="2" dy="0.6" layer="1" rot="R90"/>
</package>
<package name="BOARD_TEMPLATE_RAHS">
<wire x1="-40" y1="-45" x2="-45" y2="-40" width="0" layer="20"/>
<wire x1="-45" y1="-40" x2="-45" y2="40" width="0" layer="20"/>
<wire x1="-45" y1="40" x2="-40" y2="45" width="0" layer="20"/>
<wire x1="-40" y1="45" x2="40" y2="45" width="0" layer="20"/>
<wire x1="40" y1="45" x2="45" y2="40" width="0" layer="20"/>
<wire x1="45" y1="40" x2="45" y2="-40" width="0" layer="20"/>
<wire x1="45" y1="-40" x2="40" y2="-45" width="0" layer="20"/>
<pad name="P$1" x="-39.5" y="39.5" drill="2.9" diameter="7.5"/>
<pad name="P$2" x="-39.5" y="-39.5" drill="2.9" diameter="7.5"/>
<pad name="P$3" x="39.5" y="-39.5" drill="2.9" diameter="7.5"/>
<pad name="P$4" x="39.5" y="39.5" drill="2.9" diameter="7.5"/>
<wire x1="-33.02" y1="30.61" x2="-33.02" y2="40.77" width="0" layer="21"/>
<wire x1="-33.02" y1="40.77" x2="33.02" y2="40.77" width="0" layer="21"/>
<wire x1="33.02" y1="40.77" x2="33.02" y2="30.61" width="0" layer="21"/>
<wire x1="33.02" y1="30.61" x2="-33.02" y2="30.61" width="0" layer="21"/>
<text x="30" y="-36" size="2" layer="26" font="vector" rot="MR0">Title:
Date:
Board Template: V2.1</text>
<text x="20" y="-30" size="2" layer="26" font="vector" rot="MR0">&gt;DRAWING_NAME</text>
<text x="20" y="-33" size="2" layer="26" font="vector" rot="MR0">&gt;LAST_DATE_TIME</text>
<wire x1="-40" y1="-45" x2="-25" y2="-45" width="0" layer="20"/>
<wire x1="40" y1="-45" x2="19" y2="-45" width="0" layer="20"/>
</package>
</packages>
<symbols>
<symbol name="0545482271">
<description>Victor's Part 2</description>
<pin name="P$1" x="-20.32" y="20.32" length="middle"/>
<pin name="P$2" x="-20.32" y="17.78" length="middle"/>
<pin name="P$3" x="-20.32" y="15.24" length="middle"/>
<pin name="P$4" x="-20.32" y="12.7" length="middle"/>
<pin name="P$5" x="-20.32" y="10.16" length="middle"/>
<pin name="P$6" x="-20.32" y="7.62" length="middle"/>
<pin name="P$7" x="-20.32" y="5.08" length="middle"/>
<pin name="P$8" x="-20.32" y="2.54" length="middle"/>
<pin name="P$9" x="-20.32" y="0" length="middle"/>
<pin name="P$10" x="-20.32" y="-2.54" length="middle"/>
<pin name="P$11" x="-20.32" y="-5.08" length="middle"/>
<pin name="P$12" x="-20.32" y="-7.62" length="middle"/>
<pin name="P$13" x="-20.32" y="-10.16" length="middle"/>
<pin name="P$14" x="-20.32" y="-12.7" length="middle"/>
<pin name="P$15" x="-20.32" y="-15.24" length="middle"/>
<pin name="P$16" x="-20.32" y="-17.78" length="middle"/>
<pin name="P$17" x="-20.32" y="-20.32" length="middle"/>
<pin name="P$18" x="-20.32" y="-22.86" length="middle"/>
<pin name="P$19" x="-20.32" y="-25.4" length="middle"/>
<pin name="P$20" x="-20.32" y="-27.94" length="middle"/>
<pin name="P$21" x="-20.32" y="-30.48" length="middle"/>
<pin name="P$22" x="-20.32" y="-33.02" length="middle"/>
<wire x1="-15.24" y1="-35.56" x2="-15.24" y2="22.86" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="22.86" x2="-5.08" y2="22.86" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="22.86" x2="-5.08" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-35.56" x2="-5.08" y2="-35.56" width="0.1524" layer="94"/>
</symbol>
<symbol name="AMPHENOL_FCI_SFW15R-2STE1LF">
<description>Victor's Part</description>
<wire x1="-15.24" y1="20.32" x2="-15.24" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-25.4" x2="-5.08" y2="20.32" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="20.32" x2="-15.24" y2="20.32" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-25.4" x2="-5.08" y2="-25.4" width="0.1524" layer="94"/>
<pin name="P$1" x="-5.08" y="17.78" length="short"/>
<pin name="P$2" x="-5.08" y="15.24" length="short"/>
<pin name="P$3" x="-5.08" y="12.7" length="short"/>
<pin name="P$4" x="-5.08" y="10.16" length="short"/>
<pin name="P$5" x="-5.08" y="7.62" length="short"/>
<pin name="P$6" x="-5.08" y="5.08" length="short"/>
<pin name="P$7" x="-5.08" y="2.54" length="short"/>
<pin name="P$8" x="-5.08" y="0" length="short"/>
<pin name="P$9" x="-5.08" y="-2.54" length="short"/>
<pin name="P$10" x="-5.08" y="-5.08" length="short"/>
<pin name="P$11" x="-5.08" y="-7.62" length="short"/>
<pin name="P$12" x="-5.08" y="-10.16" length="short"/>
<pin name="P$13" x="-5.08" y="-12.7" length="short"/>
<pin name="P$14" x="-5.08" y="-15.24" length="short"/>
<pin name="P$15" x="-5.08" y="-17.78" length="short"/>
<pin name="MNT_1" x="-5.08" y="-20.32" length="short"/>
<pin name="MNT_2" x="-5.08" y="-22.86" length="short"/>
</symbol>
<symbol name="BOARD_TEMPLATE">
<text x="0" y="0" size="1.778" layer="94">Board Template: V2.1</text>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="25.4" y2="3.81" width="0.254" layer="94"/>
<wire x1="25.4" y1="3.81" x2="25.4" y2="-1.27" width="0.254" layer="94"/>
<wire x1="25.4" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0545482271">
<description>Victor's Part 2</description>
<gates>
<gate name="G$1" symbol="0545482271" x="5.08" y="2.54"/>
</gates>
<devices>
<device name="" package="0545482271">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$10" pad="10"/>
<connect gate="G$1" pin="P$11" pad="11"/>
<connect gate="G$1" pin="P$12" pad="12"/>
<connect gate="G$1" pin="P$13" pad="13"/>
<connect gate="G$1" pin="P$14" pad="14"/>
<connect gate="G$1" pin="P$15" pad="15"/>
<connect gate="G$1" pin="P$16" pad="16"/>
<connect gate="G$1" pin="P$17" pad="17"/>
<connect gate="G$1" pin="P$18" pad="18"/>
<connect gate="G$1" pin="P$19" pad="19"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$20" pad="20"/>
<connect gate="G$1" pin="P$21" pad="21"/>
<connect gate="G$1" pin="P$22" pad="22"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AMPHENOL_FCI_SFW15R-2STE1LF">
<description>Victor's Part</description>
<gates>
<gate name="G$1" symbol="AMPHENOL_FCI_SFW15R-2STE1LF" x="-17.78" y="2.54"/>
</gates>
<devices>
<device name="" package="AMPHENOL_FCI_SFW15R-2STE1LF">
<connects>
<connect gate="G$1" pin="MNT_1" pad="17"/>
<connect gate="G$1" pin="MNT_2" pad="16"/>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$10" pad="10"/>
<connect gate="G$1" pin="P$11" pad="11"/>
<connect gate="G$1" pin="P$12" pad="12"/>
<connect gate="G$1" pin="P$13" pad="13"/>
<connect gate="G$1" pin="P$14" pad="14"/>
<connect gate="G$1" pin="P$15" pad="15"/>
<connect gate="G$1" pin="P$2" pad="2"/>
<connect gate="G$1" pin="P$3" pad="3"/>
<connect gate="G$1" pin="P$4" pad="4"/>
<connect gate="G$1" pin="P$5" pad="5"/>
<connect gate="G$1" pin="P$6" pad="6"/>
<connect gate="G$1" pin="P$7" pad="7"/>
<connect gate="G$1" pin="P$8" pad="8"/>
<connect gate="G$1" pin="P$9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BOARD_TEMPLATE_RAHS">
<gates>
<gate name="G$1" symbol="BOARD_TEMPLATE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BOARD_TEMPLATE_RAHS">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rc-master-smd">
<description>&lt;b&gt;R/C MASTER-SMD! - v1.01 (07/03/2007)&lt;/b&gt;&lt;p&gt;
&lt;p&gt;This library is a collection of SMD ONLY resistors and capacitors by various manufacturers. The pad sizes, spacing and silkscreen widths have been tweaked for use in dense fine pitch layouts where space, alignment and precision are critical. In general these components are designed for routing in grid increments of 5 mils&lt;/p&gt;
&lt;p&gt;* Silkscreen line elements are a minimum of 8 mils in width. All components have text sizes of 0.032"  or 0.04".&lt;/p&gt;
&lt;p&gt;* A silkscreen text values use a ratio of 18 in all cases.&lt;/p&gt;
&lt;p&gt;* ALL PADS AND PART OUTLINES ARE SNAPPED TO A 5 MIL GRID!&lt;/p&gt;
&lt;p&gt;&lt;h4&gt;All components are prefixed using the following conventions:&lt;/h4&gt;&lt;/p&gt;
&lt;table width="380" border="1" bordercolor="#000000"&gt;
  &lt;tr&gt; 
    &lt;td width="81" bgcolor="#33CCFF"&gt;&lt;div align="center"&gt;&lt;strong&gt;Prefix&lt;/strong&gt;&lt;/div&gt;&lt;/td&gt;
    &lt;td width="289" bgcolor="#33CCFF"&gt;&lt;div align="center"&gt;&lt;strong&gt;Description&lt;/strong&gt;&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CBP_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Bipolar Electrolytic Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CCA_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Chip Cap Array Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;CP_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Polarized Electrolytic/Tantalum Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;C_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Non-Polarized Film / Chip Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;FB_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Simple Ferrite Bead Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;L_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Simple Chip Inductors&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt; 
    &lt;td&gt;&lt;div align="center"&gt;R_&lt;/div&gt;&lt;/td&gt;
    &lt;td&gt;&lt;div align="center"&gt;Resistor Types&lt;/div&gt;&lt;/td&gt;
  &lt;/tr&gt;
&lt;/table&gt;
&lt;p&gt;&lt;author&gt;THIS LIBRARY IS PROVIDED AS IS AND WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED.&lt;br&gt;Copyright (C) 2007, Bob Starr&lt;br&gt; http://www.bobstarr.net&lt;br&gt;
&lt;/author&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 0402</description>
<wire x1="-0.245" y1="0.174" x2="0.245" y2="0.174" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.174" x2="-0.245" y2="-0.174" width="0.1016" layer="51"/>
<wire x1="-1.1555" y1="0.483" x2="1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="0.483" x2="1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.1555" y1="-0.483" x2="-1.1555" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.1555" y1="-0.483" x2="-1.1555" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1.016" y1="0.508" x2="1.016" y2="0.508" width="0.2032" layer="21"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="-0.508" width="0.2032" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="-1.016" y2="-0.508" width="0.2032" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="0.508" width="0.2032" layer="21"/>
<smd name="1" x="-0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<smd name="2" x="0.508" y="0" dx="0.5" dy="0.5" layer="1"/>
<text x="-1.016" y="0.762" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.016" y="-1.27" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.254" y2="0.25" layer="51"/>
<rectangle x1="0.2588" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
<rectangle x1="-0.1" y1="-0.2" x2="0.1" y2="0.2" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 0603</description>
<wire x1="-0.432" y1="-0.306" x2="0.432" y2="-0.306" width="0.1016" layer="51"/>
<wire x1="0.432" y1="0.306" x2="-0.432" y2="0.306" width="0.1016" layer="51"/>
<wire x1="-1.473" y1="0.6655" x2="1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.6655" x2="1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.6655" x2="-1.473" y2="-0.6655" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.6655" x2="-1.473" y2="0.6655" width="0.0508" layer="39"/>
<wire x1="-1.397" y1="0.635" x2="1.397" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.397" y1="0.635" x2="1.397" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.397" y1="-0.635" x2="-1.397" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.397" y1="-0.635" x2="-1.397" y2="0.635" width="0.2032" layer="21"/>
<smd name="1" x="-0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="2" x="0.762" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1.524" y="1.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.524" y="-1.524" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
<rectangle x1="-0.8" y1="-0.4" x2="-0.4318" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.25" x2="0.1999" y2="0.25" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 0805</description>
<wire x1="-0.51" y1="0.535" x2="0.51" y2="0.535" width="0.1016" layer="51"/>
<wire x1="-0.51" y1="-0.535" x2="0.51" y2="-0.535" width="0.1016" layer="51"/>
<wire x1="-1.8143" y1="0.8243" x2="1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="0.8243" x2="1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="1.8143" y1="-0.8243" x2="-1.8143" y2="-0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8143" y1="-0.8243" x2="-1.8143" y2="0.8243" width="0.0508" layer="39"/>
<wire x1="-1.8542" y1="0.889" x2="1.8542" y2="0.889" width="0.2032" layer="21"/>
<wire x1="1.8542" y1="0.889" x2="1.8542" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="1.8542" y1="-0.889" x2="-1.8542" y2="-0.889" width="0.2032" layer="21"/>
<wire x1="-1.8542" y1="-0.889" x2="-1.8542" y2="0.889" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.2" dy="1.3" layer="1"/>
<text x="-1.778" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.778" y="-1.778" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.65" x2="1" y2="0.65" layer="51"/>
<rectangle x1="-1" y1="-0.65" x2="-0.4168" y2="0.65" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1005">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 1005</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1016" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1016" layer="51"/>
<wire x1="-0.9967" y1="0.483" x2="0.9968" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0.9968" y1="0.483" x2="0.9968" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="0.9968" y1="-0.483" x2="-0.9967" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-0.9967" y1="-0.483" x2="-0.9967" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1.27" y1="0.762" x2="1.27" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.762" x2="1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.762" x2="-1.27" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0.762" width="0.2032" layer="21"/>
<smd name="1" x="-0.635" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.635" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-1.27" y="1.016" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.27" y="-1.524" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 1206</description>
<wire x1="1.0525" y1="-0.7128" x2="-1.0652" y2="-0.7128" width="0.1016" layer="51"/>
<wire x1="1.0525" y1="0.7128" x2="-1.0652" y2="0.7128" width="0.1016" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.4731" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="0.983" x2="2.4731" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.4731" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.4892" y1="1.143" x2="2.4892" y2="1.143" width="0.2032" layer="21"/>
<wire x1="2.4892" y1="1.143" x2="2.4892" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="2.4892" y1="-1.143" x2="-2.4892" y2="-1.143" width="0.2032" layer="21"/>
<wire x1="-2.4892" y1="-1.143" x2="-2.4892" y2="1.143" width="0.2032" layer="21"/>
<smd name="2" x="1.524" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="1" x="-1.524" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-2.286" y="1.524" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.286" y="-2.032" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-0.9" y2="0.8" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<rectangle x1="0.9001" y1="-0.8" x2="1.6" y2="0.8" layer="51" rot="R180"/>
</package>
<package name="R1210">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 1210</description>
<wire x1="-2.6317" y1="1.483" x2="2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="1.483" x2="2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-1.483" x2="-2.6317" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.6317" y1="-1.483" x2="-2.6317" y2="1.483" width="0.0508" layer="39"/>
<wire x1="1.0525" y1="-1.1128" x2="-1.0652" y2="-1.1128" width="0.1016" layer="51"/>
<wire x1="1.0525" y1="1.1128" x2="-1.0652" y2="1.1128" width="0.1016" layer="51"/>
<wire x1="-2.413" y1="1.524" x2="2.413" y2="1.524" width="0.2032" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="2.413" y1="-1.524" x2="-2.413" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="-2.413" y1="-1.524" x2="-2.413" y2="1.524" width="0.2032" layer="21"/>
<smd name="2" x="1.524" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="1" x="-1.524" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.286" y="1.778" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.286" y="-2.286" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
<rectangle x1="-1.6" y1="-1.2" x2="-0.9" y2="1.2" layer="51"/>
<rectangle x1="0.9001" y1="-1.2" x2="1.6" y2="1.2" layer="51" rot="R180"/>
</package>
<package name="R2010">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 2010</description>
<wire x1="-3.4731" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.4731" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.4731" y1="-1.483" x2="-3.4731" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.662" y1="1.118" x2="1.662" y2="1.118" width="0.2032" layer="51"/>
<wire x1="-1.637" y1="-1.118" x2="1.687" y2="-1.118" width="0.2032" layer="51"/>
<wire x1="-3.6322" y1="1.651" x2="3.6322" y2="1.651" width="0.2032" layer="21"/>
<wire x1="3.6322" y1="1.651" x2="3.6322" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="3.6322" y1="-1.651" x2="-3.6322" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-3.6322" y1="-1.651" x2="-3.6322" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-2.413" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.413" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.556" y="2.032" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.556" y="-2.54" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 2012</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1016" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1016" layer="51"/>
<wire x1="-1.8143" y1="0.9831" x2="1.8142" y2="0.9831" width="0.0508" layer="39"/>
<wire x1="1.8142" y1="0.9831" x2="1.8142" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.8142" y1="-0.983" x2="-1.8143" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.8143" y1="-0.983" x2="-1.8143" y2="0.9831" width="0.0508" layer="39"/>
<wire x1="-1.9304" y1="1.016" x2="1.9304" y2="1.016" width="0.2032" layer="21"/>
<wire x1="1.9304" y1="1.016" x2="1.9304" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="1.9304" y1="-1.016" x2="-1.9304" y2="-1.016" width="0.2032" layer="21"/>
<wire x1="-1.9304" y1="-1.016" x2="-1.9304" y2="1.016" width="0.2032" layer="21"/>
<smd name="1" x="-1.016" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="1.016" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.778" y="1.27" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-1.778" y="-1.778" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 2512</description>
<wire x1="-3.973" y1="1.8243" x2="3.973" y2="1.8243" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.8243" x2="3.973" y2="-1.8242" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.8242" x2="-3.973" y2="-1.8242" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.8242" x2="-3.973" y2="1.8243" width="0.0508" layer="39"/>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.2032" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.2032" layer="51"/>
<wire x1="-4.2672" y1="1.905" x2="4.2672" y2="1.905" width="0.2032" layer="21"/>
<wire x1="4.2672" y1="1.905" x2="4.2672" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="4.2672" y1="-1.905" x2="-4.2672" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-4.2672" y1="-1.905" x2="-4.2672" y2="1.905" width="0.2032" layer="21"/>
<smd name="1" x="-3.048" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="3.048" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-4.064" y="2.286" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.064" y="-2.794" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 3216</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1016" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1016" layer="51"/>
<wire x1="-2.6318" y1="0.983" x2="2.6318" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="0.983" x2="2.6318" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-0.983" x2="-2.6318" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.6318" y1="-0.983" x2="-2.6318" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-2.667" y1="1.1684" x2="2.667" y2="1.1684" width="0.2032" layer="21"/>
<wire x1="2.667" y1="1.1684" x2="2.667" y2="-1.1684" width="0.2032" layer="21"/>
<wire x1="2.667" y1="-1.1684" x2="-2.667" y2="-1.1684" width="0.2032" layer="21"/>
<wire x1="-2.667" y1="-1.1684" x2="-2.667" y2="1.1684" width="0.2032" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.5" dy="1.8" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.5" dy="1.8" layer="1"/>
<text x="-2.54" y="1.524" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 3225</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1016" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1016" layer="51"/>
<wire x1="-2.6318" y1="1.483" x2="2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="1.483" x2="2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.6318" y1="-1.483" x2="-2.6318" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.6318" y1="-1.483" x2="-2.6318" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-2.7432" y1="1.651" x2="2.7432" y2="1.651" width="0.2032" layer="21"/>
<wire x1="2.7432" y1="1.651" x2="2.7432" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="2.7432" y1="-1.651" x2="-2.7432" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-2.7432" y1="-1.651" x2="-2.7432" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="2.032" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 5025</description>
<wire x1="-3.3143" y1="1.483" x2="3.3143" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.3143" y1="1.483" x2="3.3143" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.3143" y1="-1.483" x2="-3.3143" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.3143" y1="-1.483" x2="-3.3143" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.662" y1="1.118" x2="1.662" y2="1.118" width="0.2032" layer="51"/>
<wire x1="-1.637" y1="-1.118" x2="1.687" y2="-1.118" width="0.2032" layer="51"/>
<wire x1="-3.6322" y1="1.651" x2="3.6322" y2="1.651" width="0.2032" layer="21"/>
<wire x1="3.6322" y1="1.651" x2="3.6322" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="3.6322" y1="-1.651" x2="-3.6322" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-3.6322" y1="-1.651" x2="-3.6322" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-2.413" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.413" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.556" y="2.032" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-3.556" y="-2.54" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 6332</description>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-2.362" y1="1.346" x2="2.387" y2="1.346" width="0.2032" layer="51"/>
<wire x1="-2.362" y1="-1.346" x2="2.387" y2="-1.346" width="0.2032" layer="51"/>
<wire x1="-4.2672" y1="1.905" x2="4.2672" y2="1.905" width="0.2032" layer="21"/>
<wire x1="4.2672" y1="1.905" x2="4.2672" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="4.2672" y1="-1.905" x2="-4.2672" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-4.2672" y1="-1.905" x2="-4.2672" y2="1.905" width="0.2032" layer="21"/>
<smd name="1" x="-3.048" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="3.048" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-4.064" y="2.286" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-4.064" y="-2.794" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R0201">
<description>&lt;b&gt;SMD RESISTOR&lt;/b&gt; - 0201</description>
<wire x1="-0.195" y1="0.124" x2="0.195" y2="0.124" width="0.1016" layer="51"/>
<wire x1="0.195" y1="-0.124" x2="-0.195" y2="-0.124" width="0.1016" layer="51"/>
<wire x1="-0.8128" y1="0.4334" x2="0.8128" y2="0.4334" width="0.2032" layer="21"/>
<wire x1="0.8128" y1="0.4334" x2="0.8128" y2="-0.4318" width="0.2032" layer="21"/>
<wire x1="0.8128" y1="-0.4318" x2="-0.8128" y2="-0.4318" width="0.2032" layer="21"/>
<wire x1="-0.8128" y1="-0.4318" x2="-0.8128" y2="0.4334" width="0.2032" layer="21"/>
<smd name="1" x="-0.381" y="0" dx="0.4" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="0.381" y="0" dx="0.4" dy="0.4" layer="1" rot="R90"/>
<text x="-0.762" y="0.762" size="0.8128" layer="25" ratio="18">&gt;NAME</text>
<text x="-0.762" y="-1.27" size="0.6096" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.304" y1="-0.2" x2="-0.15" y2="0.2" layer="51"/>
<rectangle x1="0.15" y1="-0.2" x2="0.3088" y2="0.2" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-">
<wire x1="-2.54" y1="0" x2="-2.2225" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="-2.2225" y1="0.9525" x2="-1.5875" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="-1.5875" y1="-0.9525" x2="-0.9525" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="-0.9525" y1="0.9525" x2="-0.3175" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="-0.3175" y1="-0.9525" x2="0.3175" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="0.3175" y1="0.9525" x2="0.9525" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="0.9525" y1="-0.9525" x2="1.5875" y2="0.9525" width="0.2032" layer="94"/>
<wire x1="1.5875" y1="0.9525" x2="2.2225" y2="-0.9525" width="0.2032" layer="94"/>
<wire x1="2.2225" y1="-0.9525" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-2.54" y="1.5875" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="R-" x="0" y="0"/>
</gates>
<devices>
<device name="0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1005" package="R1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="led">
<description>&lt;b&gt;LEDs&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;br&gt;
Extended by Federico Battaglin &lt;author&gt;&amp;lt;federico.rd@fdpinternational.com&amp;gt;&lt;/author&gt; with DUOLED</description>
<packages>
<package name="PLCC2">
<description>&lt;b&gt;LC TOPLED®, Low Current LED,&lt;/b&gt;&lt;p&gt;
LS T679, LY T679, LG T679&lt;br&gt;
Source: http://catalog.osram-os.com .. LG_LS_LY_T679_OBS.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-1.7" dx="2.6" dy="1.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="1.7" dx="2.6" dy="1.5" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.3" y1="-2.45" x2="1.3" y2="-0.95" layer="31"/>
<rectangle x1="-1.3" y1="0.95" x2="1.3" y2="2.45" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.85" x2="1.4" y2="2.55" layer="29"/>
<rectangle x1="-1.4" y1="-2.55" x2="1.4" y2="-0.85" layer="29"/>
<rectangle x1="-1.575" y1="-1.7" x2="-1.35" y2="-0.95" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="*_T679-?-1" prefix="LED">
<description>&lt;b&gt;LC TOPLED® Low Current LED&lt;/b&gt;&lt;p&gt;
Source: http://catalog.osram-os.com .. LG_LS_LY_T679_OBS.pdf</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="D2E2" package="PLCC2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="LS">
<attribute name="COLOR" value="super-red"/>
</technology>
<technology name="LY">
<attribute name="COLOR" value="yellow"/>
</technology>
</technologies>
</device>
<device name="E1F1" package="PLCC2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="LG">
<attribute name="COLOR" value="green"/>
</technology>
</technologies>
</device>
<device name="F1G2" package="PLCC2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="LG">
<attribute name="COLOR" value="green"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DubSat1 Components latest">
<description>Shared Library (on github) for components used in Dubsat1</description>
<packages>
<package name="PC104_CONNECTOR">
<pad name="P$1" x="-31.75" y="32.134" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$2" x="-31.75" y="34.674" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$3" x="-31.75" y="37.214" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$4" x="-31.75" y="39.754" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$5" x="-29.21" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$6" x="-29.21" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$7" x="-29.21" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$8" x="-29.21" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$9" x="-26.67" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$10" x="-26.67" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$11" x="-26.67" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$12" x="-26.67" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$13" x="-24.13" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$14" x="-24.13" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$15" x="-24.13" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$16" x="-24.13" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$17" x="-21.59" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$18" x="-21.59" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$19" x="-21.59" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$20" x="-21.59" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$21" x="-19.05" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$22" x="-19.05" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$23" x="-19.05" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$24" x="-19.05" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$25" x="-16.51" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$26" x="-16.51" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$27" x="-16.51" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$28" x="-16.51" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$29" x="-13.97" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$30" x="-13.97" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$31" x="-13.97" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$32" x="-13.97" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$33" x="-11.43" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$34" x="-11.43" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$35" x="-11.43" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$36" x="-11.43" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$37" x="-8.89" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$38" x="-8.89" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$39" x="-8.89" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$40" x="-8.89" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$41" x="-6.35" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$42" x="-6.35" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$43" x="-6.35" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$44" x="-6.35" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$45" x="-3.81" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$46" x="-3.81" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$47" x="-3.81" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$48" x="-3.81" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$49" x="-1.27" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$50" x="-1.27" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$51" x="-1.27" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$52" x="-1.27" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$53" x="1.27" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$54" x="1.27" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$55" x="1.27" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$56" x="1.27" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$57" x="3.81" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$58" x="3.81" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$59" x="3.81" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$60" x="3.81" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$61" x="6.35" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$62" x="6.35" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$63" x="6.35" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$64" x="6.35" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$65" x="8.89" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$66" x="8.89" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$67" x="8.89" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$68" x="8.89" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$69" x="11.43" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$70" x="11.43" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$71" x="11.43" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$72" x="11.43" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$73" x="13.97" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$74" x="13.97" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$75" x="13.97" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$76" x="13.97" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$77" x="16.51" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$78" x="16.51" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$79" x="16.51" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$80" x="16.51" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$81" x="19.05" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$82" x="19.05" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$83" x="19.05" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$84" x="19.05" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$85" x="21.59" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$86" x="21.59" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$87" x="21.59" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$88" x="21.59" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$89" x="24.13" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$90" x="24.13" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$91" x="24.13" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$92" x="24.13" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$93" x="26.67" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$94" x="26.67" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$95" x="26.67" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$96" x="26.67" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$97" x="29.21" y="31.88" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$98" x="29.21" y="34.42" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$99" x="29.21" y="36.96" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$100" x="29.21" y="39.5" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$101" x="31.75" y="32.134" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$102" x="31.75" y="34.674" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$103" x="31.75" y="37.214" drill="0.9525" diameter="1.8796" shape="octagon"/>
<pad name="P$104" x="31.75" y="39.754" drill="0.9525" diameter="1.8796" shape="octagon"/>
<wire x1="-33.02" y1="30.61" x2="-33.02" y2="40.77" width="0.2032" layer="21"/>
<wire x1="-33.02" y1="40.77" x2="33.02" y2="40.77" width="0.2032" layer="21"/>
<wire x1="33.02" y1="40.77" x2="33.02" y2="30.61" width="0.2032" layer="21"/>
<wire x1="33.02" y1="30.61" x2="-33.02" y2="30.61" width="0.2032" layer="21"/>
<wire x1="0" y1="38.23" x2="0" y2="33.15" width="0" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PC104_CONNECTOR">
<pin name="PPT_GND@1" x="-66.04" y="30.48" length="middle"/>
<pin name="PPT_VBAT@2" x="-12.7" y="30.48" length="middle" rot="R180"/>
<pin name="PPT_GND@3" x="12.7" y="30.48" length="middle"/>
<pin name="PPT_VBAT@4" x="63.5" y="30.48" length="middle" rot="R180"/>
<pin name="PPT_VBAT@5" x="-66.04" y="27.94" length="middle"/>
<pin name="PPT_GND@6" x="-12.7" y="27.94" length="middle" rot="R180"/>
<pin name="PPT_VBAT@7" x="12.7" y="27.94" length="middle"/>
<pin name="PPT_GND@8" x="63.5" y="27.94" length="middle" rot="R180"/>
<pin name="PPT_GND@9" x="-66.04" y="25.4" length="middle"/>
<pin name="PPT_VBAT@" x="-12.7" y="25.4" length="middle" rot="R180"/>
<pin name="PPT_GND@11" x="12.7" y="25.4" length="middle"/>
<pin name="PPT_VBAT@12" x="63.5" y="25.4" length="middle" rot="R180"/>
<pin name="PPT_VBAT@13" x="-66.04" y="22.86" length="middle"/>
<pin name="PPT_GND@14" x="-12.7" y="22.86" length="middle" rot="R180"/>
<pin name="PPT_VBAT@15" x="12.7" y="22.86" length="middle"/>
<pin name="PPT_GND@16" x="63.5" y="22.86" length="middle" rot="R180"/>
<pin name="DISTRI_GND@17" x="-66.04" y="20.32" length="middle"/>
<pin name="DISTRI_VBAT@18" x="-12.7" y="20.32" length="middle" rot="R180"/>
<pin name="DISTRI_GND@19" x="12.7" y="20.32" length="middle"/>
<pin name="DISTRI_VBAT@20" x="63.5" y="20.32" length="middle" rot="R180"/>
<pin name="DISTRI_VBAT@21" x="-66.04" y="17.78" length="middle"/>
<pin name="DISTRI_GND@22" x="-12.7" y="17.78" length="middle" rot="R180"/>
<pin name="DISTRI_VBAT@23" x="12.7" y="17.78" length="middle"/>
<pin name="DISTRI_GND@24" x="63.5" y="17.78" length="middle" rot="R180"/>
<pin name="DISTRI_GND@25" x="-66.04" y="15.24" length="middle"/>
<pin name="DISTRI_VBAT@26" x="-12.7" y="15.24" length="middle" rot="R180"/>
<pin name="DISTRI_GND@27" x="12.7" y="15.24" length="middle"/>
<pin name="DISTRI_VBAT@28" x="63.5" y="15.24" length="middle" rot="R180"/>
<pin name="DISTRI_VBAT@29" x="-66.04" y="12.7" length="middle"/>
<pin name="DISTRI_GND@30" x="-12.7" y="12.7" length="middle" rot="R180"/>
<pin name="DISTRI_VBAT@31" x="12.7" y="12.7" length="middle"/>
<pin name="DISTRI_GND@32" x="63.5" y="12.7" length="middle" rot="R180"/>
<pin name="BATT_VBAT$33" x="-66.04" y="10.16" length="middle"/>
<pin name="BATT_VBATT$34" x="-12.7" y="10.16" length="middle" rot="R180"/>
<pin name="GEN_VBATT$35" x="12.7" y="10.16" length="middle"/>
<pin name="GEN_VBATT$36" x="63.5" y="10.16" length="middle" rot="R180"/>
<pin name="MT_X_A@1" x="-66.04" y="7.62" length="middle"/>
<pin name="MT_X_A@2" x="-12.7" y="7.62" length="middle" rot="R180"/>
<pin name="MT_Y_A@1" x="12.7" y="7.62" length="middle"/>
<pin name="MT_Y_A@2" x="63.5" y="7.62" length="middle" rot="R180"/>
<pin name="MT_X_B@1" x="-66.04" y="5.08" length="middle"/>
<pin name="MT_X_B@2" x="-12.7" y="5.08" length="middle" rot="R180"/>
<pin name="MT_Y_B@1" x="12.7" y="5.08" length="middle"/>
<pin name="MT_Y_B@2" x="63.5" y="5.08" length="middle" rot="R180"/>
<pin name="SYNC1" x="-66.04" y="2.54" length="middle"/>
<pin name="P$46" x="-12.7" y="2.54" length="middle" rot="R180"/>
<pin name="P$47" x="12.7" y="2.54" length="middle"/>
<pin name="P$48" x="63.5" y="2.54" length="middle" rot="R180"/>
<pin name="SYNC2" x="-66.04" y="0" length="middle"/>
<pin name="P$50" x="-12.7" y="0" length="middle" rot="R180"/>
<pin name="TOPDEPLOY+@51" x="12.7" y="0" length="middle"/>
<pin name="TOPDEPLOY-$52" x="63.5" y="0" length="middle" rot="R180"/>
<pin name="CANH" x="-66.04" y="-2.54" length="middle"/>
<pin name="P$54" x="-12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="TOPDEPLOY+@55" x="12.7" y="-2.54" length="middle"/>
<pin name="TOPDEPLOY-$56" x="63.5" y="-2.54" length="middle" rot="R180"/>
<pin name="CANL" x="-66.04" y="-5.08" length="middle"/>
<pin name="P$58" x="-12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="P@59" x="12.7" y="-5.08" length="middle"/>
<pin name="P$60" x="63.5" y="-5.08" length="middle" rot="R180"/>
<pin name="P$61" x="-66.04" y="-7.62" length="middle"/>
<pin name="P$62" x="-12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="P$63" x="12.7" y="-7.62" length="middle"/>
<pin name="P$64" x="63.5" y="-7.62" length="middle" rot="R180"/>
<pin name="P$65" x="-66.04" y="-10.16" length="middle"/>
<pin name="P$66" x="-12.7" y="-10.16" length="middle" rot="R180"/>
<pin name="P$67" x="12.7" y="-10.16" length="middle"/>
<pin name="P$68" x="63.5" y="-10.16" length="middle" rot="R180"/>
<pin name="WHEEL_VBAT@69" x="-66.04" y="-12.7" length="middle"/>
<pin name="WHEEL_VBAT@70" x="-12.7" y="-12.7" length="middle" rot="R180"/>
<pin name="WHEEL_GND@71" x="12.7" y="-12.7" length="middle"/>
<pin name="WHEEL_GND@72" x="63.5" y="-12.7" length="middle" rot="R180"/>
<pin name="P$73" x="-66.04" y="-15.24" length="middle"/>
<pin name="P$74" x="-12.7" y="-15.24" length="middle" rot="R180"/>
<pin name="P$75" x="12.7" y="-15.24" length="middle"/>
<pin name="P$76" x="63.5" y="-15.24" length="middle" rot="R180"/>
<pin name="ESTIMAT_VBAT@77" x="-66.04" y="-17.78" length="middle"/>
<pin name="ESTIMAT_VBAT@78" x="-12.7" y="-17.78" length="middle" rot="R180"/>
<pin name="ESTIMAT_GND@79" x="12.7" y="-17.78" length="middle"/>
<pin name="ESTIMAT_GND@80" x="63.5" y="-17.78" length="middle" rot="R180"/>
<pin name="P$81" x="-66.04" y="-20.32" length="middle"/>
<pin name="P$82" x="-12.7" y="-20.32" length="middle" rot="R180"/>
<pin name="P$83" x="12.7" y="-20.32" length="middle"/>
<pin name="P$84" x="63.5" y="-20.32" length="middle" rot="R180"/>
<pin name="BDOT_VBAT@85" x="-66.04" y="-22.86" length="middle"/>
<pin name="BDOT_VBAT@86" x="-12.7" y="-22.86" length="middle" rot="R180"/>
<pin name="BDOT_GND@87" x="12.7" y="-22.86" length="middle"/>
<pin name="BDOT_GND@88" x="63.5" y="-22.86" length="middle" rot="R180"/>
<pin name="P$89" x="-66.04" y="-25.4" length="middle"/>
<pin name="P$90" x="-12.7" y="-25.4" length="middle" rot="R180"/>
<pin name="P$91" x="12.7" y="-25.4" length="middle"/>
<pin name="P$92" x="63.5" y="-25.4" length="middle" rot="R180"/>
<pin name="RAHS_VBAT@93" x="-66.04" y="-27.94" length="middle"/>
<pin name="RAHS_GND@94" x="-12.7" y="-27.94" length="middle" rot="R180"/>
<pin name="P$95" x="12.7" y="-27.94" length="middle"/>
<pin name="P$96" x="63.5" y="-27.94" length="middle" rot="R180"/>
<pin name="COM2_VBAT@97" x="-66.04" y="-30.48" length="middle"/>
<pin name="COM2_GND@98" x="-12.7" y="-30.48" length="middle" rot="R180"/>
<pin name="COM2_VBAT@99" x="12.7" y="-30.48" length="middle"/>
<pin name="COM2_GND@100" x="63.5" y="-30.48" length="middle" rot="R180"/>
<pin name="COM1_VBAT@101" x="-66.04" y="-33.02" length="middle"/>
<pin name="COM1_GND@102" x="-12.7" y="-33.02" length="middle" rot="R180"/>
<pin name="COM1_VBAT@103" x="12.7" y="-33.02" length="middle"/>
<pin name="COM1_GND@104" x="63.5" y="-33.02" length="middle" rot="R180"/>
<wire x1="-60.96" y1="-35.56" x2="-60.96" y2="33.02" width="0.254" layer="94"/>
<wire x1="-60.96" y1="33.02" x2="-17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="33.02" x2="-17.78" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-35.56" x2="-60.96" y2="-35.56" width="0.254" layer="94"/>
<wire x1="17.78" y1="-35.56" x2="17.78" y2="33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="33.02" x2="58.42" y2="33.02" width="0.254" layer="94"/>
<wire x1="58.42" y1="33.02" x2="58.42" y2="-35.56" width="0.254" layer="94"/>
<wire x1="58.42" y1="-35.56" x2="17.78" y2="-35.56" width="0.254" layer="94"/>
<text x="-48.768" y="-38.862" size="1.778" layer="94">version: V2.4</text>
<wire x1="-59.69" y1="-6.35" x2="57.15" y2="-6.35" width="0" layer="98"/>
<wire x1="57.15" y1="-6.35" x2="57.15" y2="-29.21" width="0" layer="98"/>
<wire x1="57.15" y1="-29.21" x2="7.62" y2="-29.21" width="0" layer="98"/>
<wire x1="7.62" y1="-29.21" x2="7.62" y2="-26.67" width="0" layer="98"/>
<wire x1="7.62" y1="-26.67" x2="-59.69" y2="-26.67" width="0" layer="98"/>
<wire x1="-59.69" y1="-26.67" x2="-59.69" y2="-6.35" width="0" layer="98"/>
<text x="29.21" y="-10.16" size="1.778" layer="98">ADCS reserved</text>
<text x="-46.99" y="-10.16" size="1.778" layer="98">ADCS reserved</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="PC104_CONNECTOR">
<gates>
<gate name="G$1" symbol="PC104_CONNECTOR" x="-2.54" y="30.48"/>
</gates>
<devices>
<device name="" package="PC104_CONNECTOR">
<connects>
<connect gate="G$1" pin="BATT_VBAT$33" pad="P$33"/>
<connect gate="G$1" pin="BATT_VBATT$34" pad="P$34"/>
<connect gate="G$1" pin="BDOT_GND@87" pad="P$87"/>
<connect gate="G$1" pin="BDOT_GND@88" pad="P$88"/>
<connect gate="G$1" pin="BDOT_VBAT@85" pad="P$85"/>
<connect gate="G$1" pin="BDOT_VBAT@86" pad="P$86"/>
<connect gate="G$1" pin="CANH" pad="P$53"/>
<connect gate="G$1" pin="CANL" pad="P$57"/>
<connect gate="G$1" pin="COM1_GND@102" pad="P$102"/>
<connect gate="G$1" pin="COM1_GND@104" pad="P$104"/>
<connect gate="G$1" pin="COM1_VBAT@101" pad="P$101"/>
<connect gate="G$1" pin="COM1_VBAT@103" pad="P$103"/>
<connect gate="G$1" pin="COM2_GND@100" pad="P$100"/>
<connect gate="G$1" pin="COM2_GND@98" pad="P$98"/>
<connect gate="G$1" pin="COM2_VBAT@97" pad="P$97"/>
<connect gate="G$1" pin="COM2_VBAT@99" pad="P$99"/>
<connect gate="G$1" pin="DISTRI_GND@17" pad="P$17"/>
<connect gate="G$1" pin="DISTRI_GND@19" pad="P$19"/>
<connect gate="G$1" pin="DISTRI_GND@22" pad="P$22"/>
<connect gate="G$1" pin="DISTRI_GND@24" pad="P$24"/>
<connect gate="G$1" pin="DISTRI_GND@25" pad="P$25"/>
<connect gate="G$1" pin="DISTRI_GND@27" pad="P$27"/>
<connect gate="G$1" pin="DISTRI_GND@30" pad="P$30"/>
<connect gate="G$1" pin="DISTRI_GND@32" pad="P$32"/>
<connect gate="G$1" pin="DISTRI_VBAT@18" pad="P$18"/>
<connect gate="G$1" pin="DISTRI_VBAT@20" pad="P$20"/>
<connect gate="G$1" pin="DISTRI_VBAT@21" pad="P$21"/>
<connect gate="G$1" pin="DISTRI_VBAT@23" pad="P$23"/>
<connect gate="G$1" pin="DISTRI_VBAT@26" pad="P$26"/>
<connect gate="G$1" pin="DISTRI_VBAT@28" pad="P$28"/>
<connect gate="G$1" pin="DISTRI_VBAT@29" pad="P$29"/>
<connect gate="G$1" pin="DISTRI_VBAT@31" pad="P$31"/>
<connect gate="G$1" pin="ESTIMAT_GND@79" pad="P$79"/>
<connect gate="G$1" pin="ESTIMAT_GND@80" pad="P$80"/>
<connect gate="G$1" pin="ESTIMAT_VBAT@77" pad="P$77"/>
<connect gate="G$1" pin="ESTIMAT_VBAT@78" pad="P$78"/>
<connect gate="G$1" pin="GEN_VBATT$35" pad="P$35"/>
<connect gate="G$1" pin="GEN_VBATT$36" pad="P$36"/>
<connect gate="G$1" pin="MT_X_A@1" pad="P$37"/>
<connect gate="G$1" pin="MT_X_A@2" pad="P$38"/>
<connect gate="G$1" pin="MT_X_B@1" pad="P$41"/>
<connect gate="G$1" pin="MT_X_B@2" pad="P$42"/>
<connect gate="G$1" pin="MT_Y_A@1" pad="P$39"/>
<connect gate="G$1" pin="MT_Y_A@2" pad="P$40"/>
<connect gate="G$1" pin="MT_Y_B@1" pad="P$43"/>
<connect gate="G$1" pin="MT_Y_B@2" pad="P$44"/>
<connect gate="G$1" pin="P$46" pad="P$46"/>
<connect gate="G$1" pin="P$47" pad="P$47"/>
<connect gate="G$1" pin="P$48" pad="P$48"/>
<connect gate="G$1" pin="P$50" pad="P$50"/>
<connect gate="G$1" pin="P$54" pad="P$54"/>
<connect gate="G$1" pin="P$58" pad="P$58"/>
<connect gate="G$1" pin="P$60" pad="P$60"/>
<connect gate="G$1" pin="P$61" pad="P$61"/>
<connect gate="G$1" pin="P$62" pad="P$62"/>
<connect gate="G$1" pin="P$63" pad="P$63"/>
<connect gate="G$1" pin="P$64" pad="P$64"/>
<connect gate="G$1" pin="P$65" pad="P$65"/>
<connect gate="G$1" pin="P$66" pad="P$66"/>
<connect gate="G$1" pin="P$67" pad="P$67"/>
<connect gate="G$1" pin="P$68" pad="P$68"/>
<connect gate="G$1" pin="P$73" pad="P$73"/>
<connect gate="G$1" pin="P$74" pad="P$74"/>
<connect gate="G$1" pin="P$75" pad="P$75"/>
<connect gate="G$1" pin="P$76" pad="P$76"/>
<connect gate="G$1" pin="P$81" pad="P$81"/>
<connect gate="G$1" pin="P$82" pad="P$82"/>
<connect gate="G$1" pin="P$83" pad="P$83"/>
<connect gate="G$1" pin="P$84" pad="P$84"/>
<connect gate="G$1" pin="P$89" pad="P$89"/>
<connect gate="G$1" pin="P$90" pad="P$90"/>
<connect gate="G$1" pin="P$91" pad="P$91"/>
<connect gate="G$1" pin="P$92" pad="P$92"/>
<connect gate="G$1" pin="P$95" pad="P$95"/>
<connect gate="G$1" pin="P$96" pad="P$96"/>
<connect gate="G$1" pin="P@59" pad="P$59"/>
<connect gate="G$1" pin="PPT_GND@1" pad="P$1"/>
<connect gate="G$1" pin="PPT_GND@11" pad="P$11"/>
<connect gate="G$1" pin="PPT_GND@14" pad="P$14"/>
<connect gate="G$1" pin="PPT_GND@16" pad="P$16"/>
<connect gate="G$1" pin="PPT_GND@3" pad="P$3"/>
<connect gate="G$1" pin="PPT_GND@6" pad="P$6"/>
<connect gate="G$1" pin="PPT_GND@8" pad="P$8"/>
<connect gate="G$1" pin="PPT_GND@9" pad="P$9"/>
<connect gate="G$1" pin="PPT_VBAT@" pad="P$10"/>
<connect gate="G$1" pin="PPT_VBAT@12" pad="P$12"/>
<connect gate="G$1" pin="PPT_VBAT@13" pad="P$13"/>
<connect gate="G$1" pin="PPT_VBAT@15" pad="P$15"/>
<connect gate="G$1" pin="PPT_VBAT@2" pad="P$2"/>
<connect gate="G$1" pin="PPT_VBAT@4" pad="P$4"/>
<connect gate="G$1" pin="PPT_VBAT@5" pad="P$5"/>
<connect gate="G$1" pin="PPT_VBAT@7" pad="P$7"/>
<connect gate="G$1" pin="RAHS_GND@94" pad="P$94"/>
<connect gate="G$1" pin="RAHS_VBAT@93" pad="P$93"/>
<connect gate="G$1" pin="SYNC1" pad="P$45"/>
<connect gate="G$1" pin="SYNC2" pad="P$49"/>
<connect gate="G$1" pin="TOPDEPLOY+@51" pad="P$51"/>
<connect gate="G$1" pin="TOPDEPLOY+@55" pad="P$55"/>
<connect gate="G$1" pin="TOPDEPLOY-$52" pad="P$52"/>
<connect gate="G$1" pin="TOPDEPLOY-$56" pad="P$56"/>
<connect gate="G$1" pin="WHEEL_GND@71" pad="P$71"/>
<connect gate="G$1" pin="WHEEL_GND@72" pad="P$72"/>
<connect gate="G$1" pin="WHEEL_VBAT@69" pad="P$69"/>
<connect gate="G$1" pin="WHEEL_VBAT@70" pad="P$70"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Passives">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find resistors, capacitors, inductors, test points, jumper pads, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="CAP-PTH-SMALL">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="0.508" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="0.254" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="CAP-PTH-SMALL2">
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
</package>
<package name="0805">
<wire x1="-0.3" y1="0.6" x2="0.3" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-0.3" y1="-0.6" x2="0.3" y2="-0.6" width="0.1524" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.8" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-LARGE">
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-4.826" y="0" drill="0.9" diameter="1.905"/>
<pad name="2" x="4.572" y="0" drill="0.9" diameter="1.905"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="GRM43D">
<wire x1="2.25" y1="1.6" x2="1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="1.6" x2="-2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.25" y1="-1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="2.25" y2="-1.6" width="0.127" layer="51"/>
<wire x1="2.25" y1="-1.6" x2="2.25" y2="1.6" width="0.127" layer="51"/>
<wire x1="1.1" y1="1.6" x2="1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="-1.6" width="0.127" layer="51"/>
<wire x1="-2.3" y1="1.8" x2="2.3" y2="1.8" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.8" x2="2.3" y2="-1.8" width="0.127" layer="21"/>
<smd name="A" x="1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<smd name="C" x="-1.927" y="0" dx="3.2" dy="1.65" layer="1" rot="R90"/>
<text x="-2" y="2" size="0.4064" layer="25">&gt;NAME</text>
<text x="0" y="-2" size="0.4064" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.2" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="2.2" y2="1.6" layer="51"/>
</package>
<package name="0603-CAP">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.5588" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="CAP-PTH-5MM">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.7" diameter="1.651"/>
<pad name="2" x="2.5" y="0" drill="0.7" diameter="1.651"/>
<text x="-0.762" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.016" y="-1.524" size="0.4064" layer="27">&gt;Value</text>
</package>
<package name="AXIAL-5MM">
<wire x1="-1.14" y1="0.762" x2="1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0.762" x2="1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="-0.762" x2="-1.14" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="-0.762" x2="-1.14" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.14" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.14" y1="0" x2="1.394" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.14" y1="0" x2="-1.394" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-2.5" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="2.5" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.17" size="0.4" layer="25">&gt;Name</text>
<text x="-1.032" y="-0.208" size="0.4" layer="21" ratio="15">&gt;Value</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2" layer="1"/>
<text x="-0.8" y="0.5" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.9" y="-0.7" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="CTZ3">
<description>CTZ3 Series land pattern for variable capacitor - CTZ3E-50C-W1-PF</description>
<wire x1="-1.6" y1="1.4" x2="-1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-2.25" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1.6" y2="-2.25" width="0.127" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.4" x2="-1" y2="2.2" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.4" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="-1" y1="2.2" x2="1" y2="2.2" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0" x2="0.8" y2="0" width="0.127" layer="51"/>
<wire x1="-1.05" y1="2.25" x2="-1.7" y2="1.45" width="0.127" layer="21"/>
<wire x1="-1.7" y1="1.45" x2="-1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-2.35" x2="-1.05" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.05" y1="2.25" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-2.35" width="0.127" layer="21"/>
<wire x1="1.7" y1="-2.35" x2="1.05" y2="-2.35" width="0.127" layer="21"/>
<smd name="+" x="0" y="2.05" dx="1.5" dy="1.2" layer="1"/>
<smd name="-" x="0" y="-2.05" dx="1.5" dy="1.2" layer="1"/>
<text x="-2" y="3" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2" y="-3.4" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="CAP-PTH-SMALL-KIT">
<description>&lt;h3&gt;CAP-PTH-SMALL-KIT&lt;/h3&gt;
Commonly used for small ceramic capacitors. Like our 0.1uF (http://www.sparkfun.com/products/8375) or 22pF caps (http://www.sparkfun.com/products/8571).&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of this package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.667" y1="1.27" x2="2.667" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="1.27" x2="2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.667" y1="-1.27" x2="-2.667" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.667" y1="-1.27" x2="-2.667" y2="1.27" width="0.254" layer="21"/>
<pad name="1" x="-1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="2" x="1.397" y="0" drill="1.016" diameter="2.032" stop="no"/>
<polygon width="0.127" layer="30">
<vertex x="-1.4021" y="-0.9475" curve="-90"/>
<vertex x="-2.357" y="-0.0178" curve="-90.011749"/>
<vertex x="-1.4046" y="0.9576" curve="-90"/>
<vertex x="-0.4546" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-1.4046" y="-0.4395" curve="-90.012891"/>
<vertex x="-1.8491" y="-0.0153" curve="-90"/>
<vertex x="-1.4046" y="0.452" curve="-90"/>
<vertex x="-0.9627" y="-0.0051" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="1.397" y="-0.9475" curve="-90"/>
<vertex x="0.4421" y="-0.0178" curve="-90.011749"/>
<vertex x="1.3945" y="0.9576" curve="-90"/>
<vertex x="2.3445" y="-0.0204" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="1.3945" y="-0.4395" curve="-90.012891"/>
<vertex x="0.95" y="-0.0153" curve="-90"/>
<vertex x="1.3945" y="0.452" curve="-90"/>
<vertex x="1.8364" y="-0.0051" curve="-90.012967"/>
</polygon>
</package>
<package name="CAP-PTH-SMALLEZ">
<description>This is the "EZ" version of the .1" spaced ceramic thru-hole cap.&lt;br&gt;
It has reduced top mask to make it harder to put the component on the wrong side of the board.</description>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.651" stop="no"/>
<pad name="2" x="2.54" y="0" drill="0.7" diameter="1.651" stop="no"/>
<text x="-1.27" y="1.905" size="0.6096" layer="25">&gt;Name</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27">&gt;Value</text>
<circle x="0" y="0" radius="0.898025" width="0" layer="30"/>
<circle x="2.54" y="0" radius="0.915809375" width="0" layer="30"/>
<circle x="0" y="0" radius="0.40160625" width="0" layer="29"/>
<circle x="2.54" y="0" radius="0.40160625" width="0" layer="29"/>
</package>
<package name="AXIAL-0.3">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="0.762" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.143" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="1/6W-RES">
<description>1/6W Thru-hole Resistor - *UNPROVEN*</description>
<wire x1="-1.55" y1="0.85" x2="-1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.85" x2="1.55" y2="-0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.85" x2="1.55" y2="0.85" width="0.2032" layer="21"/>
<wire x1="1.55" y1="0.85" x2="-1.55" y2="0.85" width="0.2032" layer="21"/>
<pad name="1" x="-2.5" y="0" drill="0.762"/>
<pad name="2" x="2.5" y="0" drill="0.762"/>
<text x="-1.2662" y="0.9552" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.423" y="-0.4286" size="0.8128" layer="21" ratio="15">&gt;VALUE</text>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="AXIAL-0.4">
<description>1/4W Resistor, 0.4" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-3.15" y1="-1.2" x2="-3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="1.2" x2="3.15" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="1.2" x2="3.15" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-1.2" x2="-3.15" y2="-1.2" width="0.2032" layer="21"/>
<pad name="P$1" x="-5.08" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="5.08" y="0" drill="0.9" diameter="1.8796"/>
<text x="-3.175" y="1.905" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.286" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.5">
<description>1/2W Resistor, 0.5" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-4.5" y1="-1.65" x2="-4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="1.65" x2="4.5" y2="1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="1.65" x2="4.5" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-1.65" x2="-4.5" y2="-1.65" width="0.2032" layer="21"/>
<pad name="P$1" x="-6.35" y="0" drill="0.9" diameter="1.8796"/>
<pad name="P$2" x="6.35" y="0" drill="0.9" diameter="1.8796"/>
<text x="-4.445" y="2.54" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-3.429" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.6">
<description>1W Resistor, 0.6" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-5.75" y1="-2.25" x2="-5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="-5.75" y1="2.25" x2="5.75" y2="2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="2.25" x2="5.75" y2="-2.25" width="0.2032" layer="21"/>
<wire x1="5.75" y1="-2.25" x2="-5.75" y2="-2.25" width="0.2032" layer="21"/>
<pad name="P$1" x="-7.62" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="7.62" y="0" drill="1.2" diameter="1.8796"/>
<text x="-5.715" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-4.064" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.8">
<description>2W Resistor, 0.8" wide&lt;p&gt;

Yageo CFR series &lt;a href="http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf"&gt;http://www.yageo.com/pdf/yageo/Leaded-R_CFR_2008.pdf&lt;/a&gt;</description>
<wire x1="-7.75" y1="-2.5" x2="-7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="-7.75" y1="2.5" x2="7.75" y2="2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="2.5" x2="7.75" y2="-2.5" width="0.2032" layer="21"/>
<wire x1="7.75" y1="-2.5" x2="-7.75" y2="-2.5" width="0.2032" layer="21"/>
<pad name="P$1" x="-10.16" y="0" drill="1.2" diameter="1.8796"/>
<pad name="P$2" x="10.16" y="0" drill="1.2" diameter="1.8796"/>
<text x="-7.62" y="3.175" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-5.969" y="-0.381" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
<package name="AXIAL-0.3-KIT">
<description>&lt;h3&gt;AXIAL-0.3-KIT&lt;/h3&gt;

Commonly used for 1/4W through-hole resistors. 0.3" pitch between holes.&lt;br&gt;
&lt;br&gt;

&lt;b&gt;Warning:&lt;/b&gt; This is the KIT version of the AXIAL-0.3 package. This package has a smaller diameter top stop mask, which doesn't cover the diameter of the pad. This means only the bottom side of the pads' copper will be exposed. You'll only be able to solder to the bottom side.</description>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.254" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.254" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.254" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="1.016" diameter="2.032" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.159" y="-0.762" size="1.27" layer="21" font="vector" ratio="15">&gt;Value</text>
<polygon width="0.127" layer="30">
<vertex x="3.8201" y="-0.9449" curve="-90"/>
<vertex x="2.8652" y="-0.0152" curve="-90.011749"/>
<vertex x="3.8176" y="0.9602" curve="-90"/>
<vertex x="4.7676" y="-0.0178" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="3.8176" y="-0.4369" curve="-90.012891"/>
<vertex x="3.3731" y="-0.0127" curve="-90"/>
<vertex x="3.8176" y="0.4546" curve="-90"/>
<vertex x="4.2595" y="-0.0025" curve="-90.012967"/>
</polygon>
<polygon width="0.127" layer="30">
<vertex x="-3.8075" y="-0.9525" curve="-90"/>
<vertex x="-4.7624" y="-0.0228" curve="-90.011749"/>
<vertex x="-3.81" y="0.9526" curve="-90"/>
<vertex x="-2.86" y="-0.0254" curve="-90.024193"/>
</polygon>
<polygon width="0.127" layer="29">
<vertex x="-3.81" y="-0.4445" curve="-90.012891"/>
<vertex x="-4.2545" y="-0.0203" curve="-90"/>
<vertex x="-3.81" y="0.447" curve="-90"/>
<vertex x="-3.3681" y="-0.0101" curve="-90.012967"/>
</polygon>
</package>
<package name="AXIAL-0.3EZ">
<description>This is the "EZ" version of the standard .3" spaced resistor package.&lt;br&gt;
It has a reduced top mask to make it harder to install upside-down.</description>
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<pad name="P$2" x="3.81" y="0" drill="0.9" diameter="1.8796" stop="no"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25" font="vector">&gt;Name</text>
<text x="-2.032" y="-0.381" size="1.016" layer="21" font="vector" ratio="15">&gt;Value</text>
<circle x="-3.81" y="0" radius="0.508" width="0" layer="29"/>
<circle x="3.81" y="0" radius="0.523634375" width="0" layer="29"/>
<circle x="-3.81" y="0" radius="1.02390625" width="0" layer="30"/>
<circle x="3.81" y="0" radius="1.04726875" width="0" layer="30"/>
</package>
</packages>
<symbols>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="CAP-PTH-SMALL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH2" package="CAP-PTH-SMALL2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH3" package="CAP-PTH-LARGE">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="GRM43D">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH1" package="CAP-PTH-5MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_" package="AXIAL-5MM">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ASMD" package="CTZ3">
<connects>
<connect gate="G$1" pin="1" pad="+"/>
<connect gate="G$1" pin="2" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="CAP-PTH-SMALL-KIT">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="CAP-PTH-SMALLEZ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AXIAL-0.3" package="AXIAL-0.3">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/6W" package="1/6W-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/4W" package="AXIAL-0.4">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1/2W" package="AXIAL-0.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-1W" package="AXIAL-0.6">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PTH-2W" package="AXIAL-0.8">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KIT" package="AXIAL-0.3-KIT">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="EZ" package="AXIAL-0.3EZ">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="HAB">
<description>&lt;b&gt;HAB Library&lt;/b&gt;&lt;br/&gt;
&lt;br/&gt;
Parts for the High Altitude Balloon project that are not standard in EAGLE or in the SparkFun libraries</description>
<packages>
<package name="1008">
<description>1008 (2520 metric) package</description>
<wire x1="0.9" y1="1.524" x2="-0.9" y2="1.524" width="0.127" layer="51"/>
<wire x1="-0.9" y1="1.524" x2="-0.9" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-0.9" y1="-1.524" x2="0.9" y2="-1.524" width="0.127" layer="51"/>
<wire x1="0.9" y1="-1.524" x2="0.9" y2="1.524" width="0.127" layer="51"/>
<wire x1="-1" y1="-0.4" x2="-1" y2="0.4" width="0.127" layer="21"/>
<wire x1="1" y1="-0.4" x2="1" y2="0.4" width="0.127" layer="21"/>
<smd name="1" x="0" y="1.27" dx="1.7526" dy="1.2192" layer="1"/>
<smd name="2" x="0" y="-1.27" dx="1.7526" dy="1.2192" layer="1"/>
<text x="-1" y="2.667" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1" y="-3.0988" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<description>&lt;b&gt;1206&lt;/b&gt;&lt;br/&gt;
&lt;br/&gt;
Sometimes, inductors CAN come in standard packages.</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.397" y="-1.524" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="CLF10040">
<description>CLF10040 Power Inductor</description>
<wire x1="5.1562" y1="1.6256" x2="5.1562" y2="5.0038" width="0.1524" layer="21"/>
<wire x1="-5.1562" y1="-1.6256" x2="-5.1562" y2="-2.5019" width="0.1524" layer="21"/>
<wire x1="-5.1562" y1="-2.5019" x2="-5.1562" y2="-5.0038" width="0.1524" layer="21"/>
<wire x1="-5.1562" y1="2.5019" x2="-2.6543" y2="5.0038" width="0" layer="25"/>
<wire x1="-5.1562" y1="-2.5019" x2="-2.6543" y2="-5.0038" width="0" layer="25"/>
<wire x1="-5.1562" y1="2.5019" x2="-2.6543" y2="5.0038" width="0.1524" layer="21"/>
<wire x1="-5.1562" y1="-2.5019" x2="-2.6543" y2="-5.0038" width="0.1524" layer="21"/>
<wire x1="-5.1562" y1="-5.0038" x2="5.1562" y2="-5.0038" width="0.1524" layer="21"/>
<wire x1="5.1562" y1="-5.0038" x2="5.1562" y2="-1.6256" width="0.1524" layer="21"/>
<wire x1="5.1562" y1="5.0038" x2="-5.1562" y2="5.0038" width="0.1524" layer="21"/>
<wire x1="-5.1562" y1="5.0038" x2="-5.1562" y2="1.6256" width="0.1524" layer="21"/>
<wire x1="-5.1562" y1="-5.0038" x2="5.1562" y2="-5.0038" width="0" layer="25"/>
<wire x1="5.1562" y1="-5.0038" x2="5.1562" y2="5.0038" width="0" layer="25"/>
<wire x1="5.1562" y1="5.0038" x2="-5.1562" y2="5.0038" width="0" layer="25"/>
<wire x1="-5.1562" y1="5.0038" x2="-5.1562" y2="-5.0038" width="0" layer="25"/>
<smd name="1" x="-3.8989" y="0" dx="2.8194" dy="3.0988" layer="1"/>
<smd name="2" x="3.8989" y="0" dx="2.8194" dy="3.0988" layer="1"/>
</package>
<package name="MSOP8">
<description>&lt;b&gt;Micro Small Outline Package&lt;/b&gt; Grid .65mm&lt;p&gt;</description>
<wire x1="-1.4" y1="1.4986" x2="1.4" y2="1.4986" width="0.2032" layer="21"/>
<wire x1="1.4" y1="1.4986" x2="1.4" y2="-1.4986" width="0.2032" layer="21"/>
<wire x1="1.4" y1="-1.4986" x2="-1.4" y2="-1.4986" width="0.2032" layer="21"/>
<wire x1="-1.4" y1="-1.4986" x2="-1.4" y2="1.4986" width="0.2032" layer="21"/>
<circle x="-1" y="-1" radius="0.2" width="0" layer="21"/>
<smd name="1" x="-0.975" y="-2.2" dx="0.45" dy="1.45" layer="1"/>
<smd name="2" x="-0.325" y="-2.2" dx="0.45" dy="1.45" layer="1"/>
<smd name="3" x="0.325" y="-2.2" dx="0.45" dy="1.45" layer="1"/>
<smd name="4" x="0.975" y="-2.2" dx="0.45" dy="1.45" layer="1"/>
<smd name="5" x="0.975" y="2.2" dx="0.45" dy="1.45" layer="1"/>
<smd name="6" x="0.325" y="2.2" dx="0.45" dy="1.45" layer="1"/>
<smd name="7" x="-0.325" y="2.2" dx="0.45" dy="1.45" layer="1"/>
<smd name="8" x="-0.975" y="2.2" dx="0.45" dy="1.45" layer="1" rot="R180"/>
<text x="-1.785" y="3.175" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.92" y="-3.556" size="0.6096" layer="27">&gt;VALUE</text>
<rectangle x1="-1.1254" y1="-2.45" x2="-0.8254" y2="-1.5" layer="51"/>
<rectangle x1="-0.4751" y1="-2.45" x2="-0.1751" y2="-1.5" layer="51"/>
<rectangle x1="0.1751" y1="-2.45" x2="0.4751" y2="-1.5" layer="51"/>
<rectangle x1="0.8253" y1="-2.45" x2="1.1253" y2="-1.5" layer="51"/>
<rectangle x1="0.8254" y1="1.5" x2="1.1254" y2="2.45" layer="51"/>
<rectangle x1="0.1751" y1="1.5" x2="0.4751" y2="2.45" layer="51"/>
<rectangle x1="-0.4751" y1="1.5" x2="-0.1751" y2="2.45" layer="51"/>
<rectangle x1="-1.1253" y1="1.5" x2="-0.8253" y2="2.45" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="INDUCTOR">
<description>Standard inductor</description>
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="2.54" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="TPS62160">
<description>&lt;b&gt;TPS62160&lt;/b&gt;&lt;br/&gt;
&lt;br/&gt;
Adjustable output switch-mode D-CAP2 regulator</description>
<pin name="VIN" x="-12.7" y="5.08" length="short" direction="pwr"/>
<pin name="EN" x="-12.7" y="2.54" length="short" direction="in"/>
<pin name="GND" x="-12.7" y="-2.54" length="short" direction="pwr"/>
<pin name="FB" x="12.7" y="-2.54" length="short" direction="in" rot="R180"/>
<pin name="PG" x="12.7" y="0" length="short" direction="oc" rot="R180"/>
<pin name="VOS" x="12.7" y="2.54" length="short" direction="in" rot="R180"/>
<pin name="OUT" x="12.7" y="5.08" length="short" direction="out" rot="R180"/>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-10.16" y="7.62" size="1.6764" layer="95">&gt;NAME</text>
<text x="-10.16" y="-7.62" size="1.6764" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="INDUCTOR" prefix="L" uservalue="yes">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;br /&gt;
&lt;br /&gt;
Standard inductor, with some new packages&lt;br /&gt;
&lt;br /&gt;
Why do inductors never come in standard packages!?</description>
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1008" package="1008">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CLF10040" package="CLF10040">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TPS62160" prefix="U">
<description>&lt;b&gt;TPS62160&lt;/b&gt;&lt;br/&gt;
&lt;br/&gt;
Adjustable switch-mode D-CAP2 regulator</description>
<gates>
<gate name="G$1" symbol="TPS62160" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MSOP8">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="FB" pad="5"/>
<connect gate="G$1" pin="GND" pad="1 4"/>
<connect gate="G$1" pin="OUT" pad="7"/>
<connect gate="G$1" pin="PG" pad="8"/>
<connect gate="G$1" pin="VIN" pad="2"/>
<connect gate="G$1" pin="VOS" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-DiscreteSemi">
<description>&lt;h3&gt;SparkFun Electronics' preferred foot prints&lt;/h3&gt;
In this library you'll find discrete semiconductors- transistors, diodes, TRIACs, optoisolators, etc.&lt;br&gt;&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is the end user's responsibility to ensure correctness and suitablity for a given componet or application. If you enjoy using this library, please buy one of our products at www.sparkfun.com.
&lt;br&gt;&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="DIODE-ZENER">
<wire x1="-1.778" y1="0.762" x2="1.778" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.778" y1="0" x2="1.778" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="1.778" y1="-0.762" x2="-1.778" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-1.778" y1="-0.762" x2="-1.778" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="0.762" width="0.2032" layer="21"/>
<wire x1="1.778" y1="0" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.778" y1="0" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="1.778" y1="0.762" x2="1.778" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="P$1" x="-3.556" y="0" drill="0.9"/>
<pad name="P$2" x="3.556" y="0" drill="0.9"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25">&gt;Name</text>
<text x="-1.397" y="-0.508" size="0.4064" layer="21">&gt;Value</text>
</package>
<package name="SMB-DIODE">
<description>&lt;b&gt;Diode&lt;/b&gt;&lt;p&gt;
Basic small signal diode good up to 200mA. SMB footprint. Common part #: BAS16</description>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.2606" y1="1.905" x2="2.2606" y2="1.905" width="0.1016" layer="21"/>
<wire x1="-2.2606" y1="-1.905" x2="2.2606" y2="-1.905" width="0.1016" layer="21"/>
<wire x1="-2.261" y1="-1.905" x2="-2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="2.261" y1="-1.905" x2="2.261" y2="1.905" width="0.1016" layer="51"/>
<wire x1="0.643" y1="1" x2="-0.73" y2="0" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="0" x2="0.643" y2="-1" width="0.2032" layer="21"/>
<wire x1="0.643" y1="-1" x2="0.643" y2="1" width="0.2032" layer="21"/>
<wire x1="-0.73" y1="1" x2="-0.73" y2="-1" width="0.2032" layer="21"/>
<smd name="C" x="-2.2" y="0" dx="2.4" dy="2.4" layer="1"/>
<smd name="A" x="2.2" y="0" dx="2.4" dy="2.4" layer="1"/>
<text x="-2.159" y="2.159" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.429" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-1.0922" x2="-2.2606" y2="1.0922" layer="51"/>
<rectangle x1="2.2606" y1="-1.0922" x2="2.794" y2="1.0922" layer="51"/>
</package>
<package name="SOD-323">
<wire x1="-0.9" y1="0.65" x2="-0.5" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.65" x2="0.9" y2="0.65" width="0.2032" layer="21"/>
<wire x1="-0.9" y1="-0.65" x2="-0.5" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-0.65" x2="0.9" y2="-0.65" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.65" x2="-0.5" y2="-0.65" width="0.2032" layer="21"/>
<smd name="C" x="-1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<smd name="A" x="1.15" y="0" dx="0.63" dy="0.83" layer="1"/>
<text x="-0.889" y="1.016" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="SOD-123">
<description>100V/150mA 1N4148 - Super Cheap</description>
<wire x1="-1.3" y1="0.775" x2="-0.5" y2="0.775" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.775" x2="1.3" y2="0.775" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="-0.775" x2="-0.5" y2="-0.775" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-0.775" x2="1.3" y2="-0.775" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.775" x2="-0.5" y2="-0.775" width="0.2032" layer="21"/>
<smd name="C" x="-1.575" y="0" dx="0.9" dy="0.95" layer="1"/>
<smd name="A" x="1.575" y="0" dx="0.9" dy="0.95" layer="1"/>
<text x="-0.889" y="1.016" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27">&gt;VALUE</text>
<wire x1="-1.35" y1="0.775" x2="1.35" y2="0.775" width="0.127" layer="51"/>
<wire x1="1.35" y1="0.775" x2="1.35" y2="-0.775" width="0.127" layer="51"/>
<wire x1="1.35" y1="-0.775" x2="-1.35" y2="-0.775" width="0.127" layer="51"/>
<wire x1="-1.35" y1="-0.775" x2="-1.35" y2="0.775" width="0.127" layer="51"/>
</package>
<package name="PANASONIC_SMINI2-F5-B">
<wire x1="-1.3" y1="0.775" x2="-0.5" y2="0.775" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.775" x2="1.3" y2="0.775" width="0.2032" layer="21"/>
<wire x1="-1.3" y1="-0.775" x2="-0.5" y2="-0.775" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="-0.775" x2="1.3" y2="-0.775" width="0.2032" layer="21"/>
<wire x1="-0.5" y1="0.775" x2="-0.5" y2="-0.775" width="0.2032" layer="21"/>
<smd name="C" x="-1.2" y="0" dx="0.9" dy="1.1" layer="1"/>
<smd name="A" x="1.2" y="0" dx="0.9" dy="0.9" layer="1"/>
<text x="-0.889" y="1.016" size="0.4064" layer="25" font="vector">&gt;NAME</text>
<text x="-1.016" y="-1.397" size="0.4064" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.35" y1="0.775" x2="1.35" y2="0.775" width="0.127" layer="51"/>
<wire x1="1.35" y1="0.775" x2="1.35" y2="-0.775" width="0.127" layer="51"/>
<wire x1="1.35" y1="-0.775" x2="-1.35" y2="-0.775" width="0.127" layer="51"/>
<wire x1="-1.35" y1="-0.775" x2="-1.35" y2="0.775" width="0.127" layer="51"/>
</package>
<package name="DIODE-BZT52">
<wire x1="-2.54" y1="0.762" x2="2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.762" x2="2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="-2.54" y2="-0.762" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.762" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0" x2="2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.794" y2="0" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.2032" layer="21"/>
<pad name="A" x="-3.81" y="0" drill="0.9" diameter="1.8796"/>
<pad name="C" x="3.81" y="0" drill="0.9" diameter="1.8796"/>
<text x="-2.54" y="1.27" size="0.8128" layer="25" font="vector" ratio="15">&gt;Name</text>
<text x="-2.032" y="-0.508" size="0.8128" layer="21" font="vector" ratio="15">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="DIODE-ZENER">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.778" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.762" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIODE-ZENER" prefix="D">
<description>Zener Diode
Production Part - 8199
3.4V Zener Voltage</description>
<gates>
<gate name="G$1" symbol="DIODE-ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="DIODE-ZENER">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMB" package="SMB-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3.3V" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11284"/>
<attribute name="VALUE" value="3.3V" constant="no"/>
</technology>
</technologies>
</device>
<device name="16V" package="SOD-123">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-12440" constant="no"/>
<attribute name="VALUE" value="16V" constant="no"/>
</technology>
</technologies>
</device>
<device name="5.6V" package="SOD-323">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-12442" constant="no"/>
<attribute name="VALUE" value="5.6V" constant="no"/>
</technology>
</technologies>
</device>
<device name="DZ2J150M0L" package="PANASONIC_SMINI2-F5-B">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-12989" constant="no"/>
</technology>
</technologies>
</device>
<device name="" package="DIODE-BZT52">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD-ID" value="DIO-08199" constant="no"/>
<attribute name="VALUE" value="BZT52" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
<clearance class="0" value="0.1524"/>
</class>
<class number="1" name="power" width="0.2032" drill="0.6096">
<clearance class="1" value="0.1524"/>
</class>
<class number="2" name="VIN" width="0.6096" drill="0.6096">
<clearance class="2" value="0.3048"/>
</class>
<class number="3" name="usbvcc" width="0.2032" drill="0.3048">
<clearance class="3" value="0.2032"/>
</class>
</classes>
<parts>
<part name="U$2" library="DubSat1 Components" deviceset="BOARD_TEMPLATE_RAHS" device=""/>
<part name="JP2" library="RPI-Zero" deviceset="RPI-ZERO" device=""/>
<part name="U$6" library="DubSat1 Components" deviceset="AMPHENOL_FCI_SFW15R-2STE1LF" device=""/>
<part name="JP3" library="DubSat1 Components" deviceset="0545482271" device=""/>
<part name="R1" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="10k"/>
<part name="SUPPLY2" library="SparkFun-Aesthetics" deviceset="3.3V" device=""/>
<part name="C8" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value=".1u"/>
<part name="SUPPLY6" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C7" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value=".1u"/>
<part name="SUPPLY7" library="SparkFun-Aesthetics" deviceset="3.3V" device=""/>
<part name="C9" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value=".1u"/>
<part name="U$7" library="ICs" deviceset="ECS_CRYSTAL_ECX-53B-DU" device=""/>
<part name="GND1" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="GND13" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="GND16" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="U$11" library="ICs" deviceset="MICROCHIP_CAN_MCP25625_SSOP" device=""/>
<part name="GND17" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="GND18" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="C10" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="20p"/>
<part name="GND2" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="C11" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="20p"/>
<part name="GND3" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="J7" library="rc-master-smd" deviceset="R_" device="0805" value="0"/>
<part name="5V-LED" library="led" deviceset="*_T679-?-1" device="D2E2" technology="LS"/>
<part name="3V3-LED" library="led" deviceset="*_T679-?-1" device="D2E2" technology="LY"/>
<part name="R5" library="rc-master-smd" deviceset="R_" device="0805" value="330"/>
<part name="R6" library="rc-master-smd" deviceset="R_" device="0805" value="330"/>
<part name="GND4" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND5" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="JP1" library="DubSat1 Components latest" deviceset="PC104_CONNECTOR" device=""/>
<part name="U1" library="HAB" deviceset="TPS62160" device=""/>
<part name="L2" library="HAB" deviceset="INDUCTOR" device="1008" value="2.2u">
<attribute name="PARTNUM" value="CVH252009-2R2MCT-ND"/>
</part>
<part name="R2" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="430K"/>
<part name="R3" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="82K"/>
<part name="C1" library="SparkFun-Passives" deviceset="CAP" device="0805" value="10u"/>
<part name="C2" library="SparkFun-Passives" deviceset="CAP" device="0805" value="22u"/>
<part name="R4" library="SparkFun-Passives" deviceset="RESISTOR" device="0805-RES" value="10K"/>
<part name="GND6" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND7" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="D2" library="SparkFun-DiscreteSemi" deviceset="DIODE-ZENER" device="5.6V" value="5.6V">
<attribute name="PARTNO" value="DESD5V0U1BA-7DICT-ND"/>
</part>
<part name="SUPPLY3" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="C3" library="SparkFun-Passives" deviceset="CAP" device="0805" value="10u"/>
<part name="D1" library="SparkFun-DiscreteSemi" deviceset="DIODE-ZENER" device="5.6V" value="15V">
<attribute name="PARTNO" value="CDSOD323-T15SCT-ND"/>
</part>
<part name="GND8" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="GND14" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="SUPPLY4" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="GND15" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND19" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="GND20" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
<part name="J1" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="0"/>
<part name="J2" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="0"/>
<part name="J3" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="0"/>
<part name="J4" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="0"/>
<part name="J5" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="0"/>
<part name="J6" library="SparkFun-Resistors" deviceset="RESISTOR" device="0805-RES" value="0"/>
<part name="SUPPLY5" library="SparkFun-Aesthetics" deviceset="3.3V" device=""/>
<part name="SUPPLY8" library="SparkFun-Aesthetics" deviceset="3.3V" device=""/>
<part name="SUPPLY10" library="SparkFun-Aesthetics" deviceset="5V" device=""/>
<part name="U$1" library="ICs" deviceset="ANALOG_LDO_ADM7172" device=""/>
<part name=".C1" library="SparkFun-Capacitors" deviceset="CAP" device="1206" value="4.7u"/>
<part name=".C5" library="SparkFun-Capacitors" deviceset="CAP" device="0805" value="10n"/>
<part name=".C6" library="SparkFun-Capacitors" deviceset="CAP" device="1206" value="4.7u"/>
<part name="GND21" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="GND22" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="GND23" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="GND24" library="SparkFun-Aesthetics" deviceset="DGND" device=""/>
<part name="VBAT-LED" library="led" deviceset="*_T679-?-1" device="D2E2" technology="LY"/>
<part name="R7" library="rc-master-smd" deviceset="R_" device="0805" value="330"/>
<part name="GND9" library="SparkFun-Aesthetics" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-38.1" y="243.84" size="1.778" layer="97">CAN controller/transceiver</text>
<text x="330.2" y="246.38" size="1.778" layer="91">- updated PC104 conn
- added TPS regulator</text>
</plain>
<instances>
<instance part="U$2" gate="G$1" x="327.66" y="254"/>
<instance part="JP2" gate="G$1" x="185.42" y="129.54" smashed="yes">
<attribute name="NAME" x="165.1" y="165.1" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="165.1" y="163.195" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="U$6" gate="G$1" x="40.64" y="116.84"/>
<instance part="JP3" gate="G$1" x="78.74" y="114.3" rot="R180"/>
<instance part="R1" gate="G$1" x="17.78" y="185.42" rot="MR180"/>
<instance part="SUPPLY2" gate="G$1" x="27.94" y="193.04" rot="MR0"/>
<instance part="C8" gate="G$1" x="27.94" y="180.34" rot="MR0"/>
<instance part="SUPPLY6" gate="G$1" x="7.62" y="233.68"/>
<instance part="C7" gate="G$1" x="7.62" y="226.06"/>
<instance part="SUPPLY7" gate="G$1" x="-68.58" y="238.76"/>
<instance part="C9" gate="G$1" x="-68.58" y="231.14"/>
<instance part="U$7" gate="G$1" x="-101.6" y="198.12"/>
<instance part="GND1" gate="G$1" x="-71.12" y="226.06" smashed="yes"/>
<instance part="GND13" gate="G$1" x="27.94" y="172.72" smashed="yes" rot="MR0"/>
<instance part="GND16" gate="G$1" x="7.62" y="220.98" smashed="yes"/>
<instance part="U$11" gate="G$1" x="-27.94" y="236.22"/>
<instance part="GND17" gate="G$1" x="-50.8" y="213.36" smashed="yes"/>
<instance part="GND18" gate="G$1" x="-50.8" y="187.96" smashed="yes"/>
<instance part="C10" gate="G$1" x="-81.28" y="195.58"/>
<instance part="GND2" gate="G$1" x="-81.28" y="190.5" smashed="yes"/>
<instance part="C11" gate="G$1" x="-124.46" y="190.5"/>
<instance part="GND3" gate="G$1" x="-124.46" y="185.42" smashed="yes"/>
<instance part="J7" gate="G$1" x="137.16" y="165.1" rot="R270"/>
<instance part="5V-LED" gate="G$1" x="88.9" y="218.44"/>
<instance part="3V3-LED" gate="G$1" x="76.2" y="218.44"/>
<instance part="R5" gate="G$1" x="76.2" y="205.74" rot="R90"/>
<instance part="R6" gate="G$1" x="88.9" y="205.74" rot="R90"/>
<instance part="GND4" gate="1" x="76.2" y="198.12" smashed="yes"/>
<instance part="GND5" gate="1" x="88.9" y="198.12" smashed="yes"/>
<instance part="JP1" gate="G$1" x="304.8" y="124.46"/>
<instance part="U1" gate="G$1" x="287.02" y="195.58"/>
<instance part="L2" gate="G$1" x="322.58" y="200.66" smashed="yes" rot="R90">
<attribute name="NAME" x="320.04" y="203.2" size="1.778" layer="95"/>
<attribute name="VALUE" x="325.12" y="203.2" size="1.778" layer="96"/>
<attribute name="PARTNUM" x="322.58" y="200.66" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="R2" gate="G$1" x="342.9" y="193.04" rot="R90"/>
<instance part="R3" gate="G$1" x="350.52" y="185.42" rot="R180"/>
<instance part="C1" gate="G$1" x="264.16" y="193.04"/>
<instance part="C2" gate="G$1" x="355.6" y="193.04"/>
<instance part="R4" gate="G$1" x="332.74" y="193.04" rot="R90"/>
<instance part="GND6" gate="1" x="274.32" y="182.88"/>
<instance part="GND7" gate="1" x="365.76" y="182.88"/>
<instance part="D2" gate="G$1" x="365.76" y="195.58" smashed="yes" rot="R90">
<attribute name="NAME" x="368.3" y="196.0626" size="1.778" layer="95"/>
<attribute name="VALUE" x="368.3" y="193.2686" size="1.778" layer="96"/>
<attribute name="PARTNO" x="365.76" y="195.58" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="SUPPLY3" gate="G$1" x="365.76" y="203.2"/>
<instance part="C3" gate="G$1" x="378.46" y="193.04"/>
<instance part="D1" gate="G$1" x="243.84" y="195.58" smashed="yes" rot="R90">
<attribute name="NAME" x="246.38" y="196.0626" size="1.778" layer="95"/>
<attribute name="VALUE" x="246.38" y="193.2686" size="1.778" layer="96"/>
<attribute name="PARTNO" x="243.84" y="195.58" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="GND8" gate="G$1" x="-114.3" y="198.12" smashed="yes"/>
<instance part="GND14" gate="G$1" x="-88.9" y="190.5" smashed="yes"/>
<instance part="SUPPLY4" gate="G$1" x="215.9" y="160.02"/>
<instance part="GND15" gate="1" x="302.26" y="83.82" smashed="yes"/>
<instance part="GND19" gate="1" x="215.9" y="96.52" smashed="yes"/>
<instance part="GND20" gate="1" x="154.94" y="96.52" smashed="yes"/>
<instance part="J1" gate="G$1" x="-58.42" y="134.62" rot="MR180"/>
<instance part="J2" gate="G$1" x="-58.42" y="127" rot="MR180"/>
<instance part="J3" gate="G$1" x="-58.42" y="119.38" rot="MR180"/>
<instance part="J4" gate="G$1" x="-35.56" y="134.62" rot="MR180"/>
<instance part="J5" gate="G$1" x="-35.56" y="127" rot="MR180"/>
<instance part="J6" gate="G$1" x="-35.56" y="119.38" rot="MR180"/>
<instance part="SUPPLY5" gate="G$1" x="137.16" y="170.18"/>
<instance part="SUPPLY8" gate="G$1" x="76.2" y="223.52"/>
<instance part="SUPPLY10" gate="G$1" x="88.9" y="223.52"/>
<instance part="U$1" gate="G$1" x="185.42" y="210.82" rot="MR0"/>
<instance part=".C1" gate="G$1" x="157.48" y="210.82"/>
<instance part=".C5" gate="G$1" x="205.74" y="195.58"/>
<instance part=".C6" gate="G$1" x="213.36" y="210.82"/>
<instance part="GND21" gate="G$1" x="167.64" y="193.04" smashed="yes"/>
<instance part="GND22" gate="G$1" x="157.48" y="203.2" smashed="yes"/>
<instance part="GND23" gate="G$1" x="205.74" y="187.96" smashed="yes"/>
<instance part="GND24" gate="G$1" x="213.36" y="203.2" smashed="yes"/>
<instance part="VBAT-LED" gate="G$1" x="101.6" y="218.44"/>
<instance part="R7" gate="G$1" x="101.6" y="205.74" rot="R90"/>
<instance part="GND9" gate="1" x="101.6" y="198.12" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="3.3V" class="0">
<segment>
<pinref part="SUPPLY2" gate="G$1" pin="3.3V"/>
<wire x1="27.94" y1="193.04" x2="27.94" y2="190.5" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="27.94" y1="190.5" x2="27.94" y2="185.42" width="0.1524" layer="91"/>
<wire x1="27.94" y1="185.42" x2="22.86" y2="185.42" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<junction x="27.94" y="185.42"/>
<pinref part="U$11" gate="G$1" pin="VDD"/>
<wire x1="-10.16" y1="190.5" x2="27.94" y2="190.5" width="0.1524" layer="91"/>
<junction x="27.94" y="190.5"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="VIO"/>
<pinref part="SUPPLY7" gate="G$1" pin="3.3V"/>
<wire x1="-68.58" y1="236.22" x2="-68.58" y2="238.76" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<junction x="-68.58" y="236.22"/>
<wire x1="-68.58" y1="236.22" x2="-45.72" y2="236.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY5" gate="G$1" pin="3.3V"/>
<pinref part="J7" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="3V3-LED" gate="G$1" pin="A"/>
<wire x1="76.2" y1="220.98" x2="76.2" y2="223.52" width="0.1524" layer="91"/>
<pinref part="SUPPLY8" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VOUT1"/>
<wire x1="200.66" y1="218.44" x2="205.74" y2="218.44" width="0.1524" layer="91"/>
<wire x1="205.74" y1="218.44" x2="213.36" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VOUT2"/>
<wire x1="200.66" y1="213.36" x2="205.74" y2="213.36" width="0.1524" layer="91"/>
<wire x1="205.74" y1="213.36" x2="205.74" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="SENSE"/>
<wire x1="200.66" y1="208.28" x2="205.74" y2="208.28" width="0.1524" layer="91"/>
<wire x1="205.74" y1="208.28" x2="205.74" y2="213.36" width="0.1524" layer="91"/>
<junction x="205.74" y="213.36"/>
<junction x="205.74" y="218.44"/>
<pinref part=".C6" gate="G$1" pin="1"/>
<wire x1="213.36" y1="215.9" x2="213.36" y2="218.44" width="0.1524" layer="91"/>
<junction x="213.36" y="218.44"/>
<wire x1="213.36" y1="218.44" x2="223.52" y2="218.44" width="0.1524" layer="91"/>
<label x="219.456" y="220.218" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="292.1" y1="96.52" x2="302.26" y2="96.52" width="0.1524" layer="91"/>
<wire x1="302.26" y1="96.52" x2="302.26" y2="86.36" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$1" pin="RAHS_GND@94"/>
<pinref part="GND15" gate="1" pin="GND"/>
<label x="297.18" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP3" gate="G$1" pin="P$13"/>
<wire x1="99.06" y1="124.46" x2="104.14" y2="124.46" width="0.1524" layer="91"/>
<wire x1="45.72" y1="111.76" x2="45.72" y2="119.38" width="0.1524" layer="91"/>
<wire x1="45.72" y1="119.38" x2="45.72" y2="127" width="0.1524" layer="91"/>
<wire x1="45.72" y1="127" x2="45.72" y2="134.62" width="0.1524" layer="91"/>
<junction x="45.72" y="127"/>
<junction x="45.72" y="119.38"/>
<junction x="45.72" y="134.62"/>
<wire x1="45.72" y1="134.62" x2="45.72" y2="147.32" width="0.1524" layer="91"/>
<wire x1="45.72" y1="147.32" x2="66.04" y2="147.32" width="0.1524" layer="91"/>
<junction x="66.04" y="147.32"/>
<wire x1="66.04" y1="101.6" x2="66.04" y2="109.22" width="0.1524" layer="91"/>
<wire x1="66.04" y1="109.22" x2="66.04" y2="116.84" width="0.1524" layer="91"/>
<wire x1="66.04" y1="116.84" x2="66.04" y2="124.46" width="0.1524" layer="91"/>
<wire x1="66.04" y1="124.46" x2="66.04" y2="132.08" width="0.1524" layer="91"/>
<wire x1="66.04" y1="132.08" x2="66.04" y2="139.7" width="0.1524" layer="91"/>
<wire x1="66.04" y1="139.7" x2="66.04" y2="147.32" width="0.1524" layer="91"/>
<junction x="66.04" y="139.7"/>
<junction x="66.04" y="132.08"/>
<junction x="66.04" y="109.22"/>
<junction x="66.04" y="116.84"/>
<pinref part="U$6" gate="G$1" pin="P$1"/>
<wire x1="45.72" y1="134.62" x2="35.56" y2="134.62" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$4"/>
<wire x1="45.72" y1="127" x2="35.56" y2="127" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$7"/>
<wire x1="35.56" y1="119.38" x2="45.72" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$10"/>
<wire x1="35.56" y1="111.76" x2="45.72" y2="111.76" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="P$22"/>
<wire x1="99.06" y1="147.32" x2="101.6" y2="147.32" width="0.1524" layer="91"/>
<wire x1="101.6" y1="147.32" x2="101.6" y2="139.7" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="P$19"/>
<wire x1="101.6" y1="139.7" x2="99.06" y2="139.7" width="0.1524" layer="91"/>
<junction x="101.6" y="139.7"/>
<junction x="101.6" y="147.32"/>
<wire x1="101.6" y1="139.7" x2="101.6" y2="132.08" width="0.1524" layer="91"/>
<pinref part="JP3" gate="G$1" pin="P$16"/>
<wire x1="101.6" y1="132.08" x2="99.06" y2="132.08" width="0.1524" layer="91"/>
<junction x="101.6" y="132.08"/>
<pinref part="JP3" gate="G$1" pin="P$10"/>
<wire x1="101.6" y1="132.08" x2="101.6" y2="116.84" width="0.1524" layer="91"/>
<wire x1="101.6" y1="116.84" x2="99.06" y2="116.84" width="0.1524" layer="91"/>
<junction x="101.6" y="116.84"/>
<pinref part="JP3" gate="G$1" pin="P$7"/>
<wire x1="101.6" y1="116.84" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<wire x1="101.6" y1="109.22" x2="99.06" y2="109.22" width="0.1524" layer="91"/>
<junction x="101.6" y="109.22"/>
<pinref part="JP3" gate="G$1" pin="P$4"/>
<wire x1="101.6" y1="109.22" x2="101.6" y2="101.6" width="0.1524" layer="91"/>
<wire x1="101.6" y1="101.6" x2="99.06" y2="101.6" width="0.1524" layer="91"/>
<wire x1="66.04" y1="147.32" x2="101.6" y2="147.32" width="0.1524" layer="91"/>
<wire x1="66.04" y1="139.7" x2="101.6" y2="139.7" width="0.1524" layer="91"/>
<wire x1="66.04" y1="132.08" x2="101.6" y2="132.08" width="0.1524" layer="91"/>
<wire x1="66.04" y1="116.84" x2="101.6" y2="116.84" width="0.1524" layer="91"/>
<wire x1="66.04" y1="109.22" x2="101.6" y2="109.22" width="0.1524" layer="91"/>
<wire x1="66.04" y1="101.6" x2="99.06" y2="101.6" width="0.1524" layer="91"/>
<wire x1="99.06" y1="124.46" x2="66.04" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="27.94" y1="175.26" x2="27.94" y2="177.8" width="0.1524" layer="91"/>
<pinref part="GND13" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="226.06" x2="2.54" y2="226.06" width="0.1524" layer="91"/>
<wire x1="2.54" y1="226.06" x2="2.54" y2="223.52" width="0.1524" layer="91"/>
<wire x1="2.54" y1="223.52" x2="7.62" y2="223.52" width="0.1524" layer="91"/>
<pinref part="GND16" gate="G$1" pin="GND"/>
<junction x="7.62" y="223.52"/>
<pinref part="U$11" gate="G$1" pin="VSS"/>
</segment>
<segment>
<pinref part="GND17" gate="G$1" pin="GND"/>
<pinref part="U$11" gate="G$1" pin="STBY"/>
<wire x1="-50.8" y1="215.9" x2="-45.72" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND18" gate="G$1" pin="GND"/>
<pinref part="U$11" gate="G$1" pin="GND"/>
<wire x1="-50.8" y1="190.5" x2="-45.72" y2="190.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="GND1" gate="G$1" pin="GND"/>
<wire x1="-71.12" y1="228.6" x2="-68.58" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="GND@8"/>
<wire x1="210.82" y1="154.94" x2="215.9" y2="154.94" width="0.1524" layer="91"/>
<wire x1="215.9" y1="154.94" x2="215.9" y2="144.78" width="0.1524" layer="91"/>
<pinref part="GND19" gate="1" pin="GND"/>
<pinref part="JP2" gate="G$1" pin="GND@4"/>
<wire x1="215.9" y1="137.16" x2="215.9" y2="124.46" width="0.1524" layer="91"/>
<wire x1="215.9" y1="124.46" x2="215.9" y2="119.38" width="0.1524" layer="91"/>
<wire x1="215.9" y1="119.38" x2="215.9" y2="99.06" width="0.1524" layer="91"/>
<wire x1="210.82" y1="144.78" x2="215.9" y2="144.78" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="GND@5"/>
<wire x1="215.9" y1="144.78" x2="215.9" y2="137.16" width="0.1524" layer="91"/>
<wire x1="215.9" y1="137.16" x2="210.82" y2="137.16" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="GND@6"/>
<wire x1="210.82" y1="124.46" x2="215.9" y2="124.46" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="GND@7"/>
<wire x1="210.82" y1="119.38" x2="215.9" y2="119.38" width="0.1524" layer="91"/>
<junction x="215.9" y="144.78"/>
<junction x="215.9" y="137.16"/>
<junction x="215.9" y="124.46"/>
<junction x="215.9" y="119.38"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="GND@2"/>
<pinref part="GND20" gate="1" pin="GND"/>
<pinref part="JP2" gate="G$1" pin="GND@1"/>
<wire x1="154.94" y1="149.86" x2="154.94" y2="129.54" width="0.1524" layer="91"/>
<wire x1="154.94" y1="129.54" x2="154.94" y2="111.76" width="0.1524" layer="91"/>
<wire x1="154.94" y1="111.76" x2="154.94" y2="99.06" width="0.1524" layer="91"/>
<wire x1="162.56" y1="149.86" x2="154.94" y2="149.86" width="0.1524" layer="91"/>
<wire x1="162.56" y1="129.54" x2="154.94" y2="129.54" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="GND@3"/>
<wire x1="162.56" y1="111.76" x2="154.94" y2="111.76" width="0.1524" layer="91"/>
<junction x="154.94" y="129.54"/>
<junction x="154.94" y="111.76"/>
</segment>
<segment>
<pinref part="GND2" gate="G$1" pin="GND"/>
<pinref part="C10" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="GND3" gate="G$1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="274.32" y1="185.42" x2="274.32" y2="187.96" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="274.32" y1="187.96" x2="274.32" y2="193.04" width="0.1524" layer="91"/>
<wire x1="243.84" y1="187.96" x2="243.84" y2="193.04" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="243.84" y1="187.96" x2="264.16" y2="187.96" width="0.1524" layer="91"/>
<wire x1="264.16" y1="187.96" x2="264.16" y2="190.5" width="0.1524" layer="91"/>
<wire x1="264.16" y1="187.96" x2="274.32" y2="187.96" width="0.1524" layer="91"/>
<junction x="264.16" y="187.96"/>
<junction x="274.32" y="187.96"/>
<pinref part="D1" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="355.6" y1="190.5" x2="355.6" y2="187.96" width="0.1524" layer="91"/>
<wire x1="355.6" y1="187.96" x2="355.6" y2="185.42" width="0.1524" layer="91"/>
<junction x="355.6" y="187.96"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="365.76" y1="193.04" x2="365.76" y2="187.96" width="0.1524" layer="91"/>
<wire x1="365.76" y1="187.96" x2="355.6" y2="187.96" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="365.76" y1="185.42" x2="365.76" y2="187.96" width="0.1524" layer="91"/>
<junction x="365.76" y="187.96"/>
<wire x1="378.46" y1="190.5" x2="378.46" y2="187.96" width="0.1524" layer="91"/>
<wire x1="378.46" y1="187.96" x2="365.76" y2="187.96" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="P$4"/>
<pinref part="GND8" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="P$2"/>
<pinref part="GND14" gate="G$1" pin="GND"/>
<wire x1="-88.9" y1="193.04" x2="-88.9" y2="195.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="170.18" y1="208.28" x2="167.64" y2="208.28" width="0.1524" layer="91"/>
<wire x1="167.64" y1="208.28" x2="167.64" y2="198.12" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="GNDP"/>
<wire x1="167.64" y1="198.12" x2="167.64" y2="195.58" width="0.1524" layer="91"/>
<wire x1="170.18" y1="198.12" x2="167.64" y2="198.12" width="0.1524" layer="91"/>
<junction x="167.64" y="198.12"/>
</segment>
<segment>
<pinref part=".C1" gate="G$1" pin="2"/>
<wire x1="157.48" y1="205.74" x2="157.48" y2="208.28" width="0.1524" layer="91"/>
<pinref part="GND22" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part=".C5" gate="G$1" pin="2"/>
<wire x1="205.74" y1="190.5" x2="205.74" y2="193.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part=".C6" gate="G$1" pin="2"/>
<wire x1="213.36" y1="205.74" x2="213.36" y2="208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<wire x1="-10.16" y1="231.14" x2="7.62" y2="231.14" width="0.1524" layer="91"/>
<wire x1="7.62" y1="231.14" x2="7.62" y2="233.68" width="0.1524" layer="91"/>
<pinref part="SUPPLY6" gate="G$1" pin="5V"/>
<pinref part="C7" gate="G$1" pin="1"/>
<junction x="7.62" y="231.14"/>
<pinref part="U$11" gate="G$1" pin="VDDA"/>
</segment>
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="355.6" y1="200.66" x2="342.9" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="342.9" y1="200.66" x2="332.74" y2="200.66" width="0.1524" layer="91"/>
<wire x1="332.74" y1="200.66" x2="330.2" y2="200.66" width="0.1524" layer="91"/>
<wire x1="355.6" y1="198.12" x2="355.6" y2="200.66" width="0.1524" layer="91"/>
<junction x="355.6" y="200.66"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="365.76" y1="198.12" x2="365.76" y2="200.66" width="0.1524" layer="91"/>
<wire x1="365.76" y1="200.66" x2="355.6" y2="200.66" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="342.9" y1="198.12" x2="342.9" y2="200.66" width="0.1524" layer="91"/>
<junction x="342.9" y="200.66"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="332.74" y1="198.12" x2="332.74" y2="200.66" width="0.1524" layer="91"/>
<junction x="332.74" y="200.66"/>
<pinref part="U1" gate="G$1" pin="VOS"/>
<wire x1="299.72" y1="198.12" x2="332.74" y2="198.12" width="0.1524" layer="91"/>
<junction x="332.74" y="198.12"/>
<pinref part="SUPPLY3" gate="G$1" pin="5V"/>
<wire x1="365.76" y1="203.2" x2="365.76" y2="200.66" width="0.1524" layer="91"/>
<junction x="365.76" y="200.66"/>
<wire x1="378.46" y1="200.66" x2="365.76" y2="200.66" width="0.1524" layer="91"/>
<wire x1="378.46" y1="198.12" x2="378.46" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="5V0@2"/>
<wire x1="210.82" y1="157.48" x2="215.9" y2="157.48" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="5V0@1"/>
<pinref part="SUPPLY4" gate="G$1" pin="5V"/>
<wire x1="215.9" y1="160.02" x2="210.82" y2="160.02" width="0.1524" layer="91"/>
<wire x1="215.9" y1="157.48" x2="215.9" y2="160.02" width="0.1524" layer="91"/>
<junction x="215.9" y="160.02"/>
</segment>
<segment>
<pinref part="5V-LED" gate="G$1" pin="A"/>
<wire x1="88.9" y1="223.52" x2="88.9" y2="220.98" width="0.1524" layer="91"/>
<pinref part="SUPPLY10" gate="G$1" pin="5V"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VIN1"/>
<wire x1="170.18" y1="218.44" x2="165.1" y2="218.44" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="VIN2"/>
<wire x1="165.1" y1="218.44" x2="157.48" y2="218.44" width="0.1524" layer="91"/>
<wire x1="157.48" y1="218.44" x2="152.4" y2="218.44" width="0.1524" layer="91"/>
<wire x1="170.18" y1="213.36" x2="165.1" y2="213.36" width="0.1524" layer="91"/>
<wire x1="165.1" y1="213.36" x2="165.1" y2="218.44" width="0.1524" layer="91"/>
<pinref part=".C1" gate="G$1" pin="1"/>
<wire x1="157.48" y1="215.9" x2="157.48" y2="218.44" width="0.1524" layer="91"/>
<junction x="157.48" y="218.44"/>
<label x="154.94" y="221.488" size="1.778" layer="95" rot="R180"/>
<pinref part="U$1" gate="G$1" pin="EN"/>
<wire x1="170.18" y1="203.2" x2="165.1" y2="203.2" width="0.1524" layer="91"/>
<wire x1="165.1" y1="203.2" x2="165.1" y2="213.36" width="0.1524" layer="91"/>
<junction x="165.1" y="218.44"/>
<junction x="165.1" y="213.36"/>
</segment>
</net>
<net name="CAM_D0_N" class="0">
<segment>
<pinref part="JP3" gate="G$1" pin="P$21"/>
<wire x1="99.06" y1="144.78" x2="104.14" y2="144.78" width="0.1524" layer="91"/>
<wire x1="35.56" y1="132.08" x2="48.26" y2="132.08" width="0.1524" layer="91"/>
<wire x1="48.26" y1="132.08" x2="48.26" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$2"/>
<wire x1="99.06" y1="144.78" x2="48.26" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAM_D0_P" class="0">
<segment>
<wire x1="50.8" y1="129.54" x2="50.8" y2="142.24" width="0.1524" layer="91"/>
<wire x1="35.56" y1="129.54" x2="50.8" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$3"/>
<pinref part="JP3" gate="G$1" pin="P$20"/>
<wire x1="99.06" y1="142.24" x2="104.14" y2="142.24" width="0.1524" layer="91"/>
<wire x1="50.8" y1="142.24" x2="99.06" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAM_D1_N" class="0">
<segment>
<wire x1="53.34" y1="124.46" x2="53.34" y2="137.16" width="0.1524" layer="91"/>
<wire x1="35.56" y1="124.46" x2="53.34" y2="124.46" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$5"/>
<pinref part="JP3" gate="G$1" pin="P$18"/>
<wire x1="99.06" y1="137.16" x2="104.14" y2="137.16" width="0.1524" layer="91"/>
<wire x1="53.34" y1="137.16" x2="99.06" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAM_D1_P" class="0">
<segment>
<wire x1="55.88" y1="121.92" x2="55.88" y2="134.62" width="0.1524" layer="91"/>
<wire x1="35.56" y1="121.92" x2="55.88" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$6"/>
<pinref part="JP3" gate="G$1" pin="P$17"/>
<wire x1="99.06" y1="134.62" x2="104.14" y2="134.62" width="0.1524" layer="91"/>
<wire x1="55.88" y1="134.62" x2="99.06" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAM_CK_N" class="0">
<segment>
<wire x1="58.42" y1="116.84" x2="58.42" y2="129.54" width="0.1524" layer="91"/>
<wire x1="35.56" y1="116.84" x2="58.42" y2="116.84" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$8"/>
<pinref part="JP3" gate="G$1" pin="P$15"/>
<wire x1="99.06" y1="129.54" x2="104.14" y2="129.54" width="0.1524" layer="91"/>
<wire x1="58.42" y1="129.54" x2="99.06" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAM_CK_P" class="0">
<segment>
<wire x1="60.96" y1="114.3" x2="60.96" y2="127" width="0.1524" layer="91"/>
<wire x1="35.56" y1="114.3" x2="60.96" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$9"/>
<pinref part="JP3" gate="G$1" pin="P$14"/>
<wire x1="99.06" y1="127" x2="104.14" y2="127" width="0.1524" layer="91"/>
<wire x1="60.96" y1="127" x2="99.06" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAM_3V3" class="0">
<segment>
<wire x1="35.56" y1="99.06" x2="53.34" y2="99.06" width="0.1524" layer="91"/>
<wire x1="53.34" y1="99.06" x2="53.34" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$15"/>
<pinref part="JP3" gate="G$1" pin="P$1"/>
<wire x1="99.06" y1="93.98" x2="104.14" y2="93.98" width="0.1524" layer="91"/>
<wire x1="53.34" y1="93.98" x2="99.06" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAM_SDA" class="0">
<segment>
<wire x1="35.56" y1="101.6" x2="55.88" y2="101.6" width="0.1524" layer="91"/>
<wire x1="55.88" y1="101.6" x2="55.88" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$14"/>
<pinref part="JP3" gate="G$1" pin="P$2"/>
<wire x1="99.06" y1="96.52" x2="104.14" y2="96.52" width="0.1524" layer="91"/>
<wire x1="55.88" y1="96.52" x2="99.06" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAM_SCL" class="0">
<segment>
<wire x1="58.42" y1="104.14" x2="58.42" y2="99.06" width="0.1524" layer="91"/>
<wire x1="35.56" y1="104.14" x2="58.42" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$13"/>
<pinref part="JP3" gate="G$1" pin="P$3"/>
<wire x1="99.06" y1="99.06" x2="104.14" y2="99.06" width="0.1524" layer="91"/>
<wire x1="58.42" y1="99.06" x2="99.06" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAM_IO1" class="0">
<segment>
<wire x1="35.56" y1="106.68" x2="60.96" y2="106.68" width="0.1524" layer="91"/>
<wire x1="60.96" y1="106.68" x2="60.96" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$12"/>
<pinref part="JP3" gate="G$1" pin="P$5"/>
<wire x1="99.06" y1="104.14" x2="104.14" y2="104.14" width="0.1524" layer="91"/>
<wire x1="60.96" y1="104.14" x2="99.06" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAM_IO0" class="0">
<segment>
<wire x1="35.56" y1="109.22" x2="63.5" y2="109.22" width="0.1524" layer="91"/>
<wire x1="63.5" y1="109.22" x2="63.5" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="P$11"/>
<pinref part="JP3" gate="G$1" pin="P$6"/>
<wire x1="99.06" y1="106.68" x2="104.14" y2="106.68" width="0.1524" layer="91"/>
<wire x1="63.5" y1="106.68" x2="99.06" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="JP3" gate="G$1" pin="P$12"/>
<wire x1="99.06" y1="121.92" x2="104.14" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="JP3" gate="G$1" pin="P$11"/>
<wire x1="99.06" y1="119.38" x2="104.14" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="JP3" gate="G$1" pin="P$9"/>
<wire x1="99.06" y1="114.3" x2="104.14" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="JP3" gate="G$1" pin="P$8"/>
<wire x1="99.06" y1="111.76" x2="104.14" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="12.7" y1="185.42" x2="-10.16" y2="185.42" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="RESET"/>
</segment>
</net>
<net name="CANL" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="CANL"/>
<wire x1="-45.72" y1="226.06" x2="-60.96" y2="226.06" width="0.1524" layer="91"/>
<label x="-81.28" y="224.79" size="1.778" layer="95" rot="R180"/>
<label x="-60.96" y="226.06" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="CANL"/>
<wire x1="228.6" y1="119.38" x2="238.76" y2="119.38" width="0.1524" layer="91"/>
<label x="228.6" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="CANH" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="CANH"/>
<wire x1="-45.72" y1="220.98" x2="-60.96" y2="220.98" width="0.1524" layer="91"/>
<label x="-60.96" y="220.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP1" gate="G$1" pin="CANH"/>
<wire x1="238.76" y1="121.92" x2="228.6" y2="121.92" width="0.1524" layer="91"/>
<label x="228.6" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="TXD_RXCAN" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="TXD"/>
<wire x1="-10.16" y1="215.9" x2="-5.08" y2="215.9" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="215.9" x2="-5.08" y2="195.58" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="TXCAN"/>
<wire x1="-5.08" y1="195.58" x2="-10.16" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RXD_TXCAN" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="RXD"/>
<wire x1="-10.16" y1="236.22" x2="-2.54" y2="236.22" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="236.22" x2="-2.54" y2="200.66" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="RXCAN"/>
<wire x1="-2.54" y1="200.66" x2="-10.16" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<wire x1="142.24" y1="132.08" x2="162.56" y2="132.08" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="GPIO11/SCLK"/>
<label x="142.24" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="SCK"/>
<wire x1="-45.72" y1="170.18" x2="-60.96" y2="170.18" width="0.1524" layer="91"/>
<label x="-60.96" y="170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="SO" class="0">
<segment>
<wire x1="142.24" y1="134.62" x2="162.56" y2="134.62" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="GPIO9/MISO"/>
<label x="142.24" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="SO"/>
<wire x1="-10.16" y1="175.26" x2="2.54" y2="175.26" width="0.1524" layer="91"/>
<label x="-2.54" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="SI" class="0">
<segment>
<wire x1="142.24" y1="137.16" x2="162.56" y2="137.16" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$1" pin="GPIO10/MOSI"/>
<label x="142.24" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="SI"/>
<wire x1="-10.16" y1="170.18" x2="2.54" y2="170.18" width="0.1524" layer="91"/>
<label x="-2.54" y="170.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="OSC2" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="P$3"/>
<pinref part="U$11" gate="G$1" pin="OSC2"/>
<wire x1="-88.9" y1="200.66" x2="-81.28" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-81.28" y1="200.66" x2="-45.72" y2="200.66" width="0.1524" layer="91"/>
<junction x="-81.28" y="200.66"/>
</segment>
</net>
<net name="OSC1" class="0">
<segment>
<pinref part="U$7" gate="G$1" pin="P$1"/>
<wire x1="-114.3" y1="195.58" x2="-114.3" y2="187.96" width="0.1524" layer="91"/>
<wire x1="-114.3" y1="187.96" x2="-73.66" y2="187.96" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="187.96" x2="-73.66" y2="195.58" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="OSC1"/>
<wire x1="-73.66" y1="195.58" x2="-45.72" y2="195.58" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-124.46" y1="195.58" x2="-114.3" y2="195.58" width="0.1524" layer="91"/>
<junction x="-114.3" y="195.58"/>
</segment>
</net>
<net name="3V3_FROM_PI" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="3V3PI@1"/>
<wire x1="137.16" y1="160.02" x2="162.56" y2="160.02" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="2"/>
<label x="142.24" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_CS" class="0">
<segment>
<pinref part="JP2" gate="G$1" pin="!CE0!/GPIO8"/>
<wire x1="226.06" y1="132.08" x2="210.82" y2="132.08" width="0.1524" layer="91"/>
<label x="218.44" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="CS"/>
<wire x1="-10.16" y1="180.34" x2="2.54" y2="180.34" width="0.1524" layer="91"/>
<label x="-2.54" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="VBAT" class="0">
<segment>
<pinref part="JP1" gate="G$1" pin="RAHS_VBAT@93"/>
<wire x1="238.76" y1="96.52" x2="228.6" y2="96.52" width="0.1524" layer="91"/>
<label x="228.6" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VIN"/>
<wire x1="271.78" y1="200.66" x2="274.32" y2="200.66" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="EN"/>
<wire x1="274.32" y1="198.12" x2="271.78" y2="198.12" width="0.1524" layer="91"/>
<wire x1="271.78" y1="198.12" x2="271.78" y2="200.66" width="0.1524" layer="91"/>
<junction x="271.78" y="200.66"/>
<wire x1="264.16" y1="200.66" x2="271.78" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="264.16" y1="198.12" x2="264.16" y2="200.66" width="0.1524" layer="91"/>
<wire x1="264.16" y1="200.66" x2="243.84" y2="200.66" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="243.84" y1="200.66" x2="233.68" y2="200.66" width="0.1524" layer="91"/>
<wire x1="243.84" y1="198.12" x2="243.84" y2="200.66" width="0.1524" layer="91"/>
<junction x="243.84" y="200.66"/>
<label x="233.68" y="200.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="VBAT-LED" gate="G$1" pin="A"/>
<wire x1="101.6" y1="220.98" x2="101.6" y2="223.52" width="0.1524" layer="91"/>
<label x="101.6" y="223.52" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="SW5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="OUT"/>
<wire x1="314.96" y1="200.66" x2="299.72" y2="200.66" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="1"/>
<label x="302.26" y="200.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="FB5" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="345.44" y1="185.42" x2="342.9" y2="185.42" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="FB"/>
<wire x1="342.9" y1="185.42" x2="342.9" y2="187.96" width="0.1524" layer="91"/>
<wire x1="299.72" y1="193.04" x2="314.96" y2="193.04" width="0.1524" layer="91"/>
<label x="302.26" y="193.04" size="1.778" layer="95"/>
<wire x1="342.9" y1="185.42" x2="314.96" y2="185.42" width="0.1524" layer="91"/>
<wire x1="314.96" y1="185.42" x2="314.96" y2="193.04" width="0.1524" layer="91"/>
<junction x="342.9" y="185.42"/>
</segment>
</net>
<net name="PG5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PG"/>
<label x="302.26" y="195.58" size="1.778" layer="95"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="332.74" y1="187.96" x2="317.5" y2="187.96" width="0.1524" layer="91"/>
<wire x1="317.5" y1="187.96" x2="317.5" y2="195.58" width="0.1524" layer="91"/>
<wire x1="317.5" y1="195.58" x2="299.72" y2="195.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX0RTS" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="-40.64" y1="119.38" x2="-50.8" y2="119.38" width="0.1524" layer="91"/>
<label x="-50.8" y="139.7" size="1.778" layer="95" rot="R90"/>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="-50.8" y1="119.38" x2="-53.34" y2="119.38" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="119.38" x2="-50.8" y2="149.86" width="0.1524" layer="91"/>
<junction x="-50.8" y="119.38"/>
</segment>
<segment>
<pinref part="U$11" gate="G$1" pin="TX0RTS"/>
<wire x1="-10.16" y1="210.82" x2="10.16" y2="210.82" width="0.1524" layer="91"/>
<label x="0" y="210.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="TX2RTS" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="TX2RTS"/>
<wire x1="-45.72" y1="205.74" x2="-60.96" y2="205.74" width="0.1524" layer="91"/>
<label x="-60.96" y="205.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<pinref part="J4" gate="G$1" pin="1"/>
<wire x1="-53.34" y1="134.62" x2="-40.64" y2="134.62" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="134.62" x2="-40.64" y2="149.86" width="0.1524" layer="91"/>
<junction x="-40.64" y="134.62"/>
<label x="-40.64" y="139.7" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="RX1BF" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="RX1BF"/>
<wire x1="-45.72" y1="185.42" x2="-60.96" y2="185.42" width="0.1524" layer="91"/>
<label x="-60.96" y="185.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="-63.5" y1="134.62" x2="-63.5" y2="127" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="1"/>
<junction x="-63.5" y="119.38"/>
<wire x1="-63.5" y1="127" x2="-63.5" y2="119.38" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="119.38" x2="-63.5" y2="109.22" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="1"/>
<junction x="-63.5" y="127"/>
<label x="-63.5" y="109.22" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="RX0BF" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="RX0BF"/>
<wire x1="-45.72" y1="180.34" x2="-60.96" y2="180.34" width="0.1524" layer="91"/>
<label x="-60.96" y="180.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="134.62" x2="-30.48" y2="127" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="2"/>
<junction x="-30.48" y="127"/>
<wire x1="-30.48" y1="127" x2="-30.48" y2="119.38" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="2"/>
<junction x="-30.48" y="119.38"/>
<wire x1="-30.48" y1="119.38" x2="-30.48" y2="109.22" width="0.1524" layer="91"/>
<label x="-30.48" y="109.22" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="INT_PIN" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="INT"/>
<wire x1="-45.72" y1="175.26" x2="-60.96" y2="175.26" width="0.1524" layer="91"/>
<label x="-60.96" y="175.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="JP2" gate="G$1" pin="GPIO27/GEN2"/>
<wire x1="162.56" y1="144.78" x2="142.24" y2="144.78" width="0.1524" layer="91"/>
<label x="142.24" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="5V-LED" gate="G$1" pin="C"/>
<wire x1="88.9" y1="210.82" x2="88.9" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="3V3-LED" gate="G$1" pin="C"/>
<wire x1="76.2" y1="210.82" x2="76.2" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX1RTS" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="TX1RTS"/>
<wire x1="-45.72" y1="210.82" x2="-60.96" y2="210.82" width="0.1524" layer="91"/>
<label x="-60.96" y="210.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<pinref part="J5" gate="G$1" pin="1"/>
<wire x1="-53.34" y1="127" x2="-45.72" y2="127" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="127" x2="-40.64" y2="127" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="127" x2="-45.72" y2="149.86" width="0.1524" layer="91"/>
<junction x="-45.72" y="127"/>
<label x="-45.72" y="139.7" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="SS"/>
<pinref part=".C5" gate="G$1" pin="1"/>
<wire x1="200.66" y1="203.2" x2="205.74" y2="203.2" width="0.1524" layer="91"/>
<wire x1="205.74" y1="203.2" x2="205.74" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="VBAT-LED" gate="G$1" pin="C"/>
<wire x1="101.6" y1="210.82" x2="101.6" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="102,1,210.82,160.02,5V0,5V,,,,"/>
<approved hash="102,1,162.56,160.02,3V3PI,3V3_FROM_PI,,,,"/>
<approved hash="102,1,210.82,157.48,5V0,5V,,,,"/>
<approved hash="201,1,162.56,160.02,3V3PI,3V3_FROM_PI\, 3V3PI,,,,"/>
<approved hash="104,1,274.32,200.66,U1,VIN,VBAT,,,"/>
<approved hash="117,1,162.56,139.7,3V3PI@2,,,,,"/>
<approved hash="106,1,-10.16,180.34,CS,,,,,"/>
<approved hash="106,1,99.06,121.92,N$18,,,,,"/>
<approved hash="106,1,99.06,119.38,N$19,,,,,"/>
<approved hash="106,1,99.06,114.3,N$20,,,,,"/>
<approved hash="106,1,99.06,111.76,N$21,,,,,"/>
<approved hash="106,1,210.82,132.08,SPI_CS,,,,,"/>
<approved hash="108,1,157.48,208.28,3.3V,,,,,"/>
<approved hash="108,1,66.04,124.46,GND,,,,,"/>
<approved hash="108,1,99.06,124.46,GND,,,,,"/>
<approved hash="108,1,99.06,101.6,GND,,,,,"/>
<approved hash="108,1,99.06,144.78,CAM_D0_N,,,,,"/>
<approved hash="108,1,99.06,142.24,CAM_D0_P,,,,,"/>
<approved hash="108,1,99.06,137.16,CAM_D1_N,,,,,"/>
<approved hash="108,1,99.06,134.62,CAM_D1_P,,,,,"/>
<approved hash="108,1,99.06,129.54,CAM_CK_N,,,,,"/>
<approved hash="108,1,99.06,127,CAM_CK_P,,,,,"/>
<approved hash="108,1,99.06,93.98,CAM_3V3,,,,,"/>
<approved hash="108,1,99.06,96.52,CAM_SDA,,,,,"/>
<approved hash="108,1,99.06,99.06,CAM_SCL,,,,,"/>
<approved hash="108,1,99.06,104.14,CAM_IO1,,,,,"/>
<approved hash="108,1,99.06,106.68,CAM_IO0,,,,,"/>
<approved hash="108,1,264.16,200.66,VBAT,,,,,"/>
<approved hash="111,1,157.48,203.2,3.3V,,,,,"/>
<approved hash="111,1,96.52,109.22,GND,,,,,"/>
<approved hash="111,1,96.52,116.84,GND,,,,,"/>
<approved hash="111,1,96.52,132.08,GND,,,,,"/>
<approved hash="111,1,96.52,139.7,GND,,,,,"/>
<approved hash="111,1,96.52,147.32,GND,,,,,"/>
<approved hash="115,1,101.6,124.46,GND,,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
